import LatLonVincenty from 'geodesy/latlon-vincenty';
import LatLon, { intersection } from 'geodesy/latlon-spherical';
import Leaflet from 'leaflet';

const INTERSECT_LNG = 179.999;
const MIN_STEPS = 5;

const datum = {
  ellipsoid: {
    a: 6378137,
    b: 6356752.3142,
    f: 1 / 298.257223563,
  },
};

var foo = {
  options: {
    dash: 1,
  },

  calculateDistance (coords1, coords2) {
    const point1 = new LatLon(...coords1, datum);
    const point2 = new LatLon(...coords2, datum);

    return point1.distanceTo(point2);
  },

  _generate_Geodesic: function (latlngs) {
    const path = [];
    let _geocnt = 0;
    const distance = this.calculateDistance(...latlngs[0]);
    const steps = MIN_STEPS + (distance / 250000);

    for (let poly = 0; poly < latlngs.length; poly++) {
      path[_geocnt] = [];
      let prev = Leaflet.latLng(latlngs[poly][0]);
      for (let points = 0; points < (latlngs[poly].length - 1); points++) {
        // use prev, so that wrapping behaves correctly
        let pointA = prev;
        let pointB = Leaflet.latLng(latlngs[poly][points + 1]);

        const curr = latlngs[poly][points + 1];
        const pointA1 = new LatLonVincenty(latlngs[poly][0][0], latlngs[poly][0][1], datum);
        const pointB1 = new LatLonVincenty(curr[0], curr[1], datum);

        if (pointA.equals(pointB)) continue;

        const inverse = pointA1.inverse(pointB1);

        path[_geocnt].push(prev);
        for (let s = 1; s <= steps;) {
          let distance = inverse.distance / steps;
          // dashed lines don't go the full distance between the points
          const distMult = s - 1 + this.options.dash;

          const { point: gp } = pointA1.direct(distance * distMult, inverse.initialBearing);

          if (Math.abs(gp.lng - prev.lng) > 180) {
            const sec = intersection(new LatLon(pointA1.lat, pointA1.lon), inverse.initialBearing, new LatLon(
              -89,
              ((gp.lng - prev.lng) > 0) ? -INTERSECT_LNG : INTERSECT_LNG
            ), 0);

            if (sec) {
              path[_geocnt].push(Leaflet.latLng(sec.lat, sec.lng));
              _geocnt++;
              path[_geocnt] = [];
              prev = Leaflet.latLng(sec.lat, -sec.lng);
              path[_geocnt].push(prev);
            } else {
              _geocnt++;
              path[_geocnt] = [];
              path[_geocnt].push(gp);
              prev = gp;
              s++;
            }
          } else {
            path[_geocnt].push(gp);

            // Dashed lines start a new line
            if (this.options.dash < 1) {
              _geocnt++;
              // go full distance this time, to get starting point for next line

              let { point: newPrev } = pointA1.direct(distance * s, inverse.initialBearing);

              path[_geocnt] = [];
              prev = newPrev;
              path[_geocnt].push(prev);
            } else {
              prev = gp;
            }

            s++;
          }
        }
      }
      _geocnt++;
    }

    return {
      distance,
      path,
    };
  },
};

export default foo._generate_Geodesic.bind(foo);
