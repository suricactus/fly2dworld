import calculateGeodesic from './geodesic';

export default class Flight {
  constructor (fromAirport, toAirport) {
    this.cacheKey = `${fromAirport}|${toAirport}`;
    this.cacheKeyReversed = `${toAirport}|${fromAirport}`;
    this.trips = [];
    this.connections = [];
    this.connectionTrips = [];
    this.backConnections = [];
    this.flightIds = [];
    this.tripIds = [];
    this.fromAirport = fromAirport;
    this.toAirport = toAirport;
    this.isDirectOnly = true;
    this.hasDirect = false;

    this.connectionIds = [];
    this.tripIds = [];
    this.connectionTripIds = [];

    const geodesic = this.getGeodesic();

    this.geodesic = geodesic.path;
    this.distance = geodesic.distance;
  }

  static CACHE = {};

  getGeodesic () {
    if (!Flight.CACHE[ this.cacheKey ]) {
      if (Flight.CACHE[ this.cacheKeyReversed ]) {
        const path = [Flight.CACHE[ this.cacheKeyReversed ].path[0].reverse()];

        console.log(Flight);

        Flight.CACHE[ this.cacheKey ] = {
          distance: Flight.CACHE[ this.cacheKeyReversed ],
          path,
        };
      } else {
        const { lat: startLat, lng: startLng } = window.airports[ this.fromAirport ];
        const { lat: endLat, lng: endLng } = window.airports[ this.toAirport ];

        Flight.CACHE[ this.cacheKey ] = calculateGeodesic([[[startLat, startLng], [endLat, endLng]]]);
      }
    }

    return Flight.CACHE[ this.cacheKey ];
  }
}
