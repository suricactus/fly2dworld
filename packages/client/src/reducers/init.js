import {
  FETCH_INIT,
  FETCH_INIT_SUCCESS,
  FETCH_INIT_ERROR,
} from '../actions/init';

const initialState = {
  isLoading: true,
  isReady: false,
  error: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_INIT:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FETCH_INIT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isReady: true,
      };
    case FETCH_INIT_ERROR:
      return {
        ...state,
        isLoading: false,
        isReady: true,
        error: action.error,
      };
    default:
      return state;
  }
};
