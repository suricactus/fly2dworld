import { combineReducers } from 'redux';
import init from './init';
import airlines from './airlines';
import airports from './airports';
import filters from './filters';
import subscriptions from './subscriptions';
import mapOptions from './mapOptions';
import mapCanvas from './mapCanvas';
import trips from './trips';
import tripConnections from './tripConnections';

export default combineReducers({
  init,
  airlines,
  airports,
  filters,
  subscriptions,
  mapOptions,
  mapCanvas,
  trips,
  tripConnections,
});
