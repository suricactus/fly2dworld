import {
  FETCH_TRIP_CONNECTIONS,
  FETCH_TRIP_CONNECTIONS_SUCCESS,
  FETCH_TRIP_CONNECTIONS_ERROR,
} from '../actions/tripConnections';
import {
  FETCH_TRIPS,
} from '../actions/trips';

const initialState = {
  isLoading: false,
  isReady: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRIPS:
      return {
        ...state,
        isLoading: false,
        isReady: false,
        data: undefined,
        error: undefined,
      };

    case FETCH_TRIP_CONNECTIONS:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FETCH_TRIP_CONNECTIONS_SUCCESS:
    case FETCH_TRIP_CONNECTIONS_ERROR:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isReady: true,
      };
    default:
      return state;
  }
};
