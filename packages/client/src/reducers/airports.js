import {
  FETCH_AIRPORTS,
  FETCH_AIRPORTS_SUCCESS,
  FETCH_AIRPORTS_ERROR,
} from '../actions/airports';

const initialState = {
  isLoading: false,
  isReady: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_AIRPORTS:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FETCH_AIRPORTS_SUCCESS:
    case FETCH_AIRPORTS_ERROR:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isReady: true,
      };
    default:
      return state;
  }
};
