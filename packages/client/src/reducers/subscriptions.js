import {
  FETCH_SUBSCRIPTIONS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_ERROR,
} from '../actions/subscriptions';

const initialState = {
  isLoading: false,
  isReady: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUBSCRIPTIONS:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FETCH_SUBSCRIPTIONS_SUCCESS:
    case FETCH_SUBSCRIPTIONS_ERROR:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isReady: true,
      };
    default:
      return state;
  }
};
