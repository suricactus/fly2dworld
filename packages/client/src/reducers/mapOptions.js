import {
  SET_SHOW_LINES_AS,
  SET_CAPITALS_ONLY,
  SET_GROUP_BY_CITY,
  SET_ISOCHRONE_MAP,
  SET_CATEGORIZE_COUNTRIES,
  SET_CATEGORIZE_COUNTRIES_ATTRIBUTE,
  SET_CONNECTIONS_RANGE,
  SET_ANIMATIONS_ENABLED,
} from '../actions/mapOptions';

const initialState = {
  connectionsRange: [1, 10],
  isCategorizeCountriesEnabled: false,
  categorizeCountriesAttribute: 'price',
  isIsochroneMapEnabled: false,
  isCapitalsOnlyEnabled: false,
  showLinesAs: 'flights',
  isGroupByCityEnabled: false,
  isAnimated: !true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CONNECTIONS_RANGE:
      return {
        ...state,
        connectionsRange: action.value,
      };
    case SET_CATEGORIZE_COUNTRIES:
      return {
        ...state,
        isCategorizeCountriesEnabled: action.value,
      };
    case SET_CATEGORIZE_COUNTRIES_ATTRIBUTE:
      return {
        ...state,
        categorizeCountriesAttribute: action.value,
      };
    case SET_ISOCHRONE_MAP:
      return {
        ...state,
        isIsochroneMapEnabled: action.value,
      };
    case SET_CAPITALS_ONLY:
      return {
        ...state,
        isCapitalsOnlyEnabled: action.value,
      };
    case SET_SHOW_LINES_AS:
      return {
        ...state,
        showLinesAs: action.value,
      };
    case SET_GROUP_BY_CITY:
      return {
        ...state,
        isGroupByCityEnabled: action.value,
      };
    case SET_ANIMATIONS_ENABLED:
      return {
        ...state,
        isAnimated: action.value,
      };
    default:
      return state;
  }
};
