import {
  FETCH_TRIPS,
  FETCH_TRIPS_SUCCESS,
  FETCH_TRIPS_ERROR,
  FINISH_TRIPS_PARSING,
} from '../actions/trips';

const initialState = {
  isLoading: false,
  isReady: false,
  trips: new Map(),
  connections: new Map(),
  flights: new Map(),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRIPS:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FINISH_TRIPS_PARSING:
      return {
        ...state,
        trips: action.trips,
        connections: action.connections,
        flights: action.flights,
      };
    case FETCH_TRIPS_SUCCESS:
    case FETCH_TRIPS_ERROR:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isReady: true,
      };
    default:
      return state;
  }
};
