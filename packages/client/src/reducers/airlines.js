import {
  FETCH_AIRLINES,
  FETCH_AIRLINES_SUCCESS,
  FETCH_AIRLINES_ERROR,
} from '../actions/airlines';

const initialState = {
  isLoading: false,
  isReady: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_AIRLINES:
      return {
        ...state,
        isLoading: true,
        isReady: false,
      };
    case FETCH_AIRLINES_SUCCESS:
    case FETCH_AIRLINES_ERROR:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isReady: true,
      };
    default:
      return state;
  }
};
