import {
  SET_FILTER_SUBSCRIPTION,
  SET_FILTER_ORIGIN,
  SET_FILTER_DATE,
  SET_FILTER_SEARCHED_FOR,
} from '../actions/filters';

const initialState = {
  subscriptionId: null,
  origin: null,
  date: (new Date()).toISOString().split('T')[0],
  searchAttribute: 'price',
  searchAttributesList: [{
    name: 'best',
    title: 'Best',
  }, {
    name: 'price',
    title: 'Price',
  }, {
    name: 'duration',
    title: 'Duration',
  }],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER_SUBSCRIPTION:
      return {
        ...state,
        subscriptionId: action.value,
      };
    case SET_FILTER_ORIGIN:
      return {
        ...state,
        origin: action.value,
      };
    case SET_FILTER_DATE:
      return {
        ...state,
        date: action.value,
      };
    case SET_FILTER_SEARCHED_FOR:
      return {
        ...state,
        searchAttribute: action.value,
      };
    default:
      return state;
  }
};
