import {
  SET_HOVERED_FLIGHT,
  SET_FOCUSED_FLIGHT,
} from '../actions/mapCanvas';

const initialState = {
  hoveredFlight: null,
  focusedFlight: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_HOVERED_FLIGHT:
      return {
        ...state,
        hoveredFlight: action.hoveredFlight,
      };
    case SET_FOCUSED_FLIGHT:
      return {
        ...state,
        focusedFlight: action.focusedFlight,
      };
    default:
      return state;
  }
};
