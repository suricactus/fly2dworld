import axios from 'axios';
import assert from 'assert';
import moment from 'moment';
import { takeEvery, takeLatest, call, put, select } from 'redux-saga/effects';

import Flight from '../lib/Flight';

import {
  FETCH_INIT,
  fetchInitSuccess,
} from '../actions/init';
import {
  SET_FILTER_SUBSCRIPTION,
  SET_FILTER_ORIGIN,
  SET_FILTER_DATE,
  SET_FILTER_SEARCHED_FOR,
  setFilterSubscription,
  setFilterOrigin,
} from '../actions/filters';
import {
  fetchSubscriptions,
  fetchSubscriptionsSuccess,
  fetchSubscriptionsError,
} from '../actions/subscriptions';
import {
  fetchAirports,
  fetchAirportsSuccess,
  fetchAirportsError,
} from '../actions/airports';
import {
  fetchCountriesGeojson,
  fetchCountriesGeojsonSuccess,
  fetchCountriesGeojsonError,
} from '../actions/countriesGeojson';
import {
  SELECT_TRIP,
  FETCH_TRIPS_SUCCESS,
  fetchTrips,
  fetchTripsSuccess,
  fetchTripsError,
  finishTripsParsing,
} from '../actions/trips';
import {
  fetchTripConnections,
  fetchTripConnectionsSuccess,
  fetchTripConnectionsError,
} from '../actions/tripConnections';

const allowedMethods = ['post', 'get'];
const client = axios.create({
  baseURL: 'http://10.21.2.50:2000/',
  responseType: 'json',
});
const callApi = ({ method = 'get', url = '', rest, payload = {} }) => {
  method = method.toLowerCase();

  assert(allowedMethods.includes(method));

  if (rest) {
    url = rest.map(p => encodeURIComponent(p)).join('/');
  }

  return client(url, payload).then(({ data }) => ({ payload: { data } }));
};

const selectSubscriptions = ({ subscriptions }) => subscriptions;
const selectAirports = ({ airports }) => airports.data;
const selectFilterValues = ({ filters }) => ({ ...filters });
const selectSelectedTripId = ({ selectedTripId: tripId }) => ({ tripId });
const selectTrips = ({ trips }) => trips.data;

const fetchAsync = function * ({ $$actions, ...params }) {
  const [onAction, onSuccess, onError] = $$actions;
  try {
    yield put(onAction());

    const resp = yield call(callApi, params);

    yield put(onSuccess(resp));
  } catch (error) {
    yield put(onError({ error }));
  }
};

function * fetchInitAsync () {
  yield * fetchAsync({
    url: 'subscriptions',
    $$actions: [fetchSubscriptions, fetchSubscriptionsSuccess, fetchSubscriptionsError],
  });

  const subscrs = yield select(selectSubscriptions);

  // TODO handle this
  if (subscrs.data.length === 0) throw new Error('No idea what to do!');

  const defaultSubscr = subscrs.data[0];

  yield put(setFilterSubscription({ value: defaultSubscr.id }));
  yield put(setFilterOrigin({ value: defaultSubscr.airports_list }));

  yield * fetchAsync({
    url: 'airports/all',
    $$actions: [fetchAirports, fetchAirportsSuccess, fetchAirportsError],
  });

  window.airports = yield select(selectAirports);

  console.log(window.airports)

  try {
    yield put(fetchCountriesGeojson);

    const client = axios.create({
      baseURL: '/',
      responseType: 'json',
    });

    const resp = yield call(client, 'countries.geo.json');

    yield put(fetchCountriesGeojsonSuccess(resp));
  } catch (error) {
    yield put(fetchCountriesGeojsonError({ error }));
  }

  yield put(fetchInitSuccess());
}

const CACHE_TRIPS = new Map();
const CACHE_FLIGHTS = new Map();
const CACHE_CONNECTIONS = new Map();

function * fetchTripsAsync () {
  const filters = yield select(selectFilterValues);

  try {
    yield * fetchAsync({
      rest: ['subscriptions', filters.subscriptionId, 'trips'],
      payload: {
        ...filters,
      },
      $$actions: [() => fetchTrips(filters), fetchTripsSuccess, fetchTripsError],
    });
  } catch (e) {
    console.log(111, e);
  }
}

function * fetchTripConnectionsAsync () {
  const { tripId } = yield select(selectSelectedTripId);

  yield * fetchAsync({
    rest: ['trips', tripId, 'connections'],
    onSuccess: fetchTripConnectionsSuccess,
    $$actions: [() => fetchTripConnections({ tripId }), fetchTripConnectionsSuccess, fetchTripConnectionsError],
  });
}

function * finishTripsParsingAsync () {
  const trips = yield select(selectTrips);

  for (const rawTrip of trips) {
    if (CACHE_TRIPS.get(rawTrip.id)) continue;

    const trip = {
      id: rawTrip.id,
      price: rawTrip.price,
      totalDistance: 0,
      connections: [],
      airportFrom: rawTrip.airport_from,
      airportTo: rawTrip.airport_to,
      bookingToken: rawTrip.booking_token,
      key: `${rawTrip.airport_from}|${rawTrip.airport_to}`,
    };
    const flight = CACHE_FLIGHTS.get(trip.key) || new Flight(trip.airportFrom, trip.airportTo);

    CACHE_FLIGHTS.set(trip.key, flight);

    trip.flight = flight;

    for (const rawConnection of rawTrip.connections) {
      if (CACHE_CONNECTIONS.get(rawConnection.id)) continue;

      const connection = {
        id: rawConnection.id,
        tripId: trip.id,
        airportFrom: rawConnection.airport_from,
        airportTo: rawConnection.airport_to,
        key: `${rawConnection.airport_from}|${rawConnection.airport_to}`,
        arrivesAt: rawConnection.arrives_at,
        airline: rawConnection.airline,
        departsAt: rawConnection.departs_at,
        flightNumber: rawConnection.flight_number,
        duration: moment(rawConnection.arrives_at).diff(rawConnection.departs_at),
      };
      const flight = CACHE_FLIGHTS.get(connection.key) || new Flight(connection.airportFrom, connection.airportTo);

      CACHE_FLIGHTS.set(connection.key, flight);

      flight.connectionIds.push(connection.id);
      flight.connectionTripIds.push(trip.id);

      connection.flight = flight;

      trip.totalDistance += flight.distance;
      trip.connections.push(connection.id);

      CACHE_CONNECTIONS.set(connection.id, connection);
    }

    trip.flight.connectionIds.push(trip.id);

    CACHE_TRIPS.set(trip.id, trip);
  }

  yield put(finishTripsParsing({
    trips: new Map(CACHE_TRIPS),
    flights: new Map(CACHE_FLIGHTS),
    connections: new Map(CACHE_CONNECTIONS),
  }));
}

function * rootSaga () {
  yield takeLatest(FETCH_INIT, fetchInitAsync);
  yield takeLatest([
    SET_FILTER_SUBSCRIPTION,
    SET_FILTER_ORIGIN,
    SET_FILTER_DATE,
    SET_FILTER_SEARCHED_FOR,
  ], fetchTripsAsync);
  yield takeLatest(SELECT_TRIP, fetchTripConnectionsAsync);
  yield takeLatest(FETCH_TRIPS_SUCCESS, finishTripsParsingAsync);
}

export default rootSaga;
