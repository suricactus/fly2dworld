import React from 'react';
import ReactDOM from 'react-dom';

import { Provider, connect } from 'react-redux';

import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import store from './stores/index';

const ConnectedApp = connect(({ dispatch, init }) => {
  return {
    dispatch,
    initSuccess: init.isReady,
  };
})(App);

const provider = (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
);

ReactDOM.render(provider, document.getElementById('root'));
registerServiceWorker();
