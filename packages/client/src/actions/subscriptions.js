export const FETCH_SUBSCRIPTIONS = 'FETCH_SUBSCRIPTIONS';
export const FETCH_SUBSCRIPTIONS_SUCCESS = 'FETCH_SUBSCRIPTIONS_SUCCESS';
export const FETCH_SUBSCRIPTIONS_ERROR = 'FETCH_SUBSCRIPTIONS_ERROR';

export const fetchSubscriptions = () => {
  return {
    type: FETCH_SUBSCRIPTIONS,
  };
};

export const fetchSubscriptionsSuccess = ({ payload }) => {
  return {
    type: FETCH_SUBSCRIPTIONS_SUCCESS,
    payload,
  };
};

export const fetchSubscriptionsError = ({ payload }) => {
  return {
    type: FETCH_SUBSCRIPTIONS_ERROR,
    payload,
  };
};
