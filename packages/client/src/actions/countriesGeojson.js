export const FETCH_COUNTIRIES_GEOJSON = 'FETCH_COUNTIRIES_GEOJSON';
export const FETCH_COUNTIRIES_GEOJSON_SUCCESS = 'FETCH_COUNTIRIES_GEOJSON_SUCCESS';
export const FETCH_COUNTIRIES_GEOJSON_ERROR = 'FETCH_COUNTIRIES_GEOJSON_ERROR';

export const fetchCountriesGeojson = () => {
  return {
    type: FETCH_COUNTIRIES_GEOJSON,
  };
};

export const fetchCountriesGeojsonSuccess = ({ payload }) => {
  return {
    type: FETCH_COUNTIRIES_GEOJSON_SUCCESS,
    payload,
  };
};

export const fetchCountriesGeojsonError = ({ payload }) => {
  return {
    type: FETCH_COUNTIRIES_GEOJSON_ERROR,
    payload,
  };
};
