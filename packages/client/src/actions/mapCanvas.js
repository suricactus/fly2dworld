export const SET_HOVERED_FLIGHT = 'SET_HOVERED_FLIGHT';
export const SET_FOCUSED_FLIGHT = 'SET_FOCUSED_FLIGHT';

export const setHoveredFlight = ({ flight }) => {
  return {
    type: SET_HOVERED_FLIGHT,
    hoveredFlight: flight,
  };
};

export const setFocusedFlight = ({ flight }) => {
  return {
    type: SET_FOCUSED_FLIGHT,
    focusedFlight: flight,
  };
};
