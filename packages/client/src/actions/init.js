export const FETCH_INIT = 'FETCH_INIT';
export const FETCH_INIT_SUCCESS = 'FETCH_INIT_SUCCESS';
export const FETCH_INIT_ERROR = 'FETCH_INIT_ERROR';

export const fetchInit = () => {
  return {
    type: FETCH_INIT,
  };
};

export const fetchInitSuccess = () => {
  return {
    type: FETCH_INIT_SUCCESS,
  };
};
