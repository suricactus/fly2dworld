export const FETCH_TRIP_CONNECTIONS = 'FETCH_TRIP_CONNECTIONS';
export const FETCH_TRIP_CONNECTIONS_SUCCESS = 'FETCH_TRIP_CONNECTIONS_SUCCESS';
export const FETCH_TRIP_CONNECTIONS_ERROR = 'FETCH_TRIP_CONNECTIONS_ERROR';

export const fetchTripConnections = () => {
  return {
    type: FETCH_TRIP_CONNECTIONS,
  };
};

export const fetchTripConnectionsSuccess = ({ payload }) => {
  return {
    type: FETCH_TRIP_CONNECTIONS_SUCCESS,
    payload,
  };
};

export const fetchTripConnectionsError = ({ payload }) => {
  return {
    type: FETCH_TRIP_CONNECTIONS_ERROR,
    payload,
  };
};
