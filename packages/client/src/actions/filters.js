export const SET_FILTER_SUBSCRIPTION = 'SET_FILTER_SUBSCRIPTION';
export const SET_FILTER_ORIGIN = 'SET_FILTER_ORIGIN';
export const SET_FILTER_DATE = 'SET_FILTER_DATE';
export const SET_FILTER_SEARCHED_FOR = 'SET_FILTER_SEARCHED_FOR';

export const setFilterSubscription = ({ value }) => {
  return {
    type: SET_FILTER_SUBSCRIPTION,
    value,
  };
};

export const setFilterOrigin = ({ value }) => {
  return {
    type: SET_FILTER_ORIGIN,
    value,
  };
};

export const setFilterDate = ({ value }) => {
  return {
    type: SET_FILTER_DATE,
    value,
  };
};

export const setFilterSearchAttribute = ({ value }) => {
  return {
    type: SET_FILTER_SEARCHED_FOR,
    value,
  };
};
