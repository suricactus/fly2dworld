export const FETCH_AIRLINES = 'FETCH_AIRLINES';
export const FETCH_AIRLINES_SUCCESS = 'FETCH_AIRLINES_SUCCESS';
export const FETCH_AIRLINES_ERROR = 'FETCH_AIRLINES_ERROR';

export const fetchAirlines = () => {
  return {
    type: FETCH_AIRLINES,
  };
};

export const fetchAirlinesSuccess = ({ payload }) => {
  return {
    type: FETCH_AIRLINES_SUCCESS,
    payload,
  };
};

export const fetchAirlinesError = ({ payload }) => {
  return {
    type: FETCH_AIRLINES_ERROR,
    payload,
  };
};
