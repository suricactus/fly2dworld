export const FETCH_TRIPS = 'FETCH_TRIPS';
export const FETCH_TRIPS_SUCCESS = 'FETCH_TRIPS_SUCCESS';
export const FETCH_TRIPS_ERROR = 'FETCH_TRIPS_ERROR';
export const SELECT_TRIP = 'SELECT_TRIP';
export const FINISH_TRIPS_PARSING = 'FINISH_TRIPS_PARSING';
export const FOCUS_TRIP = 'SELECT_TRIP';

export const fetchTrips = () => {
  return {
    type: FETCH_TRIPS,
  };
};

export const fetchTripsSuccess = ({ payload }) => {
  return {
    type: FETCH_TRIPS_SUCCESS,
    payload,
  };
};

export const fetchTripsError = ({ payload }) => {
  return {
    type: FETCH_TRIPS_ERROR,
    payload,
  };
};

export const finishTripsParsing = ({ trips, flights, connections }) => {
  return {
    type: FINISH_TRIPS_PARSING,
    trips,
    flights,
    connections,
  };
};

export const selectTrip = ({ tripId }) => {
  return {
    type: FOCUS_TRIP,
    tripId,
  };
};

export const focusTrip = ({ tripId }) => {
  return {
    type: FOCUS_TRIP,
    tripId,
  };
};
