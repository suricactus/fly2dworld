export const FETCH_AIRPORTS = 'FETCH_AIRPORTS';
export const FETCH_AIRPORTS_SUCCESS = 'FETCH_AIRPORTS_SUCCESS';
export const FETCH_AIRPORTS_ERROR = 'FETCH_AIRPORTS_ERROR';

export const fetchAirports = () => {
  return {
    type: FETCH_AIRPORTS,
  };
};

export const fetchAirportsSuccess = ({ payload }) => {
  return {
    type: FETCH_AIRPORTS_SUCCESS,
    payload,
  };
};

export const fetchAirportsError = ({ payload }) => {
  return {
    type: FETCH_AIRPORTS_ERROR,
    payload,
  };
};