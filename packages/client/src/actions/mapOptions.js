export const SET_SHOW_LINES_AS = 'SET_SHOW_LINES_AS';
export const SET_CAPITALS_ONLY = 'SET_CAPITALS_ONLY';
export const SET_GROUP_BY_CITY = 'SET_GROUP_BY_CITY';
export const SET_ISOCHRONE_MAP = 'SET_ISOCHRONE_MAP';
export const SET_CATEGORIZE_COUNTRIES = 'SET_CATEGORIZE_COUNTRIES';
export const SET_CATEGORIZE_COUNTRIES_ATTRIBUTE = 'SET_CATEGORIZE_COUNTRIES_ATTRIBUTE';
export const SET_CONNECTIONS_RANGE = 'SET_CONNECTIONS_RANGE';
export const SET_ANIMATIONS_ENABLED = 'SET_ANIMATIONS_ENABLED';

export const setShowLinesAs = ({ value }) => {
  return {
    type: SET_SHOW_LINES_AS,
    value,
  };
};

export const setCapitalsOnly = ({ value }) => {
  return {
    type: SET_CAPITALS_ONLY,
    value,
  };
};

export const setGroupByCity = ({ value }) => {
  return {
    type: SET_GROUP_BY_CITY,
    value,
  };
};

export const setIsochroneMap = ({ value }) => {
  return {
    type: SET_ISOCHRONE_MAP,
    value,
  };
};

export const setCategorizeCountries = ({ value }) => {
  return {
    type: SET_CATEGORIZE_COUNTRIES,
    value,
  };
};

export const setCategorizeCountriesAttribute = ({ value }) => {
  return {
    type: SET_CATEGORIZE_COUNTRIES_ATTRIBUTE,
    value,
  };
};

export const setConnectionsRange = ({ value }) => {
  return {
    type: SET_CONNECTIONS_RANGE,
    value,
  };
};

export const setAnimationsEnabled = ({ value }) => {
  return {
    type: SET_ANIMATIONS_ENABLED,
    value,
  };
};
