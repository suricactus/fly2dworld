import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import Bullet from './labels/Bullet';
import LabelAirline from './labels/LabelAirline';
import LabelDeparts from './labels/LabelDeparts';
import LabelArrives from './labels/LabelArrives';
import LabelDuration from './labels/LabelDuration';
import LabelDistance from './labels/LabelDistance';
import TitleOriginDestination from './labels/TitleOriginDestination';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  connectionDetails: {
    display: 'flex',
    alignContent: 'stretch',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
  },
});

const __connections = [
  {
    id: 111,
    from_airport_code: 'SOF',
    to_airport_code: 'ATH',
    airline_code: 'BA',
    flight_number: '7635',
    departs_at: '2018-05-05T20:45',
    arrives_at: '2018-05-06T01:30',
  },
  {
    id: 112,
    from_airport_code: 'SOF',
    to_airport_code: 'ATH',
    airline_code: 'BA',
    flight_number: '7635',
    departs_at: '2018-05-07T16:20',
    arrives_at: '2018-05-07T17:10',
  },
];

const ConnectionData = withStyles(styles)(({ classes, connection }) => (
  <span>
    <span className={classes.connectionDetails}>
      <LabelDuration
        departsAt={connection.departs_at}
        arrivesAt={connection.arrives_at}
      />
      <Bullet />
      <LabelDeparts timestamp={connection.departs_at} />
      <Bullet />
      <LabelArrives timestamp={connection.arrives_at} />
      <Bullet />
      <LabelDistance distance={Math.round(Math.random() * 1000)}
        from={connection.from_airport_code}
        to={connection.to_airport_code}
      />
    </span>
    <span className={classes.connectionDetails}>
      <LabelAirline
        code={connection.airline_code}
        flightNumber={connection.flight_number}
      />
    </span>
  </span>
));

const FlightsList = ({ classes, flights }) => (
  <div className={classes.root}>
    <List>
      {flights.reduce((d, f, idx) => [
        ...d,
        idx === 0 ? null : <Divider key={`d${idx}`} component="li" />,
        (
          <ListItem key={`l${idx}`}>
            <ListItemText
              primary={
                <TitleOriginDestination
                  from={f.from_airport_code}
                  to={f.to_airport_code}
                />
              }
              secondary={<ConnectionData flight={f} />}
            />
          </ListItem>
        ),
      ])}
    </List>
  </div>
);

FlightsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FlightsList);
