import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import ConnectionList from './ConnectionList';
import BookButton from './BookButton';
import TitleOriginDestination from './labels/TitleOriginDestination';
import LabelChanges from './labels/LabelChanges';
import LabelPrice from './labels/LabelPrice';
import LabelDuration from './labels/LabelDuration';


const styles = theme => ({
  root: {
    width: '100%',
    display: 'block',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '40%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  verticalFlex: {
    '& > *': {
      flexDirection: 'column',
    },
  },
  verticalFlex2: {
    padding: 0,
    flexDirection: 'column',
  },
  subheader: {
    display: 'flex',
    alignContent: 'stretch',
    flexFlow: 'row nowrap',
    justifyContent: '',
  },
  bookBtn: {
    right: theme.spacing.unit * 6,
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
  },
});

class TripListItem extends React.Component {
  state = {
    isExpanded: false,
  }

  handleChange (event) {
    this.setState({ isExpanded: !!this.state.isExpanded });
  }

  render () {
    const { classes, trip } = this.props;
    const { isExpanded } = this.state;

    const firstFlight = trip.connections[0];
    const lastFlight = trip.connections[ trip.connections.length - 1 ];
    const flightDuration = moment(lastFlight.arrivesAt).diff(firstFlight.departsAt);

    return (
      <ExpansionPanel expanded={isExpanded} onChange={e => this.handleChange(e)}>
        <ExpansionPanelSummary className={classes.verticalFlex} expandIcon={<ExpandMoreIcon />}>
          <TitleOriginDestination
            className={classes.heading}
            from={trip.airportFrom}
            to={trip.airportTo}
          />
          <BookButton token={trip.bookingToken} />
          <div className={classes.subheader}>
            <LabelDuration className={classes.secondaryHeading} duration={flightDuration} />
            <LabelChanges count={trip.connections.length - 1} />
            <LabelPrice price={trip.price} />
          </div>
          {/*
          <ConnectionList />
        */}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.verticalFlex2}>
          <Typography className={classes.title} color="textSecondary">
            Time: flying {trip.flight_time}, on land {trip.land_time}
          </Typography>
          <Typography className={classes.title} color="textSecondary">
            Distance: direct {trip.flight.distance}, total: {trip.totalDistance}
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

TripListItem.propTypes = {
  trip: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TripListItem);
