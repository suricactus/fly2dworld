import React from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';
import { Range } from 'rc-slider';

import 'rc-slider/assets/index.css';

import {
  setShowLinesAs,
  setCapitalsOnly,
  setGroupByCity,
  setIsochroneMap,
  setCategorizeCountries,
  setCategorizeCountriesAttribute,
  setConnectionsRange,
  setAnimationsEnabled,
} from '../actions/mapOptions';

const ITEM_HEIGHT = 48;

const styles = theme => ({
  formControl: {},
  group: {},
});

const mapStateToProps = ({
  mapOptions,
}) => ({
  mapOptions,
});

const mapDispatchToProps = {
  setShowLinesAs,
  setCapitalsOnly,
  setGroupByCity,
  setIsochroneMap,
  setCategorizeCountries,
  setCategorizeCountriesAttribute,
  setConnectionsRange,
  setAnimationsEnabled,
};

class LongMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleShowLinesAsChange = (e, value) => {
    this.props.setShowLinesAs({ value });
  };

  handleCapitalsOnlyChange = (e, value) => {
    this.props.setCapitalsOnly({ value });
  };

  handleGroupByCityChange = (e, value) => {
    this.props.setGroupByCity({ value });
  };

  handleIsochroneMapChange = (e, value) => {
    this.props.setIsochroneMap({ value });
  };

  handleCategorizeCountriesChange = (e, value) => {
    this.props.setCategorizeCountries({ value });
  };

  handleCategorizeCountryAttributeChange = (e, value) => {
    this.props.setCategorizeCountriesAttribute({ value });
  };

  handleAnimationsChange = (e, value) => {
    this.props.setAnimationsEnabled({ value });
  };

  handleConnectionsRangeChange = (value) => {
    this.props.setConnectionsRange({ value });
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render () {
    const { anchorEl } = this.state;
    const { mapOptions } = this.props;

    return (
      <div>
        <IconButton
          aria-label="More"
          aria-owns={anchorEl ? 'long-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem>
            <FormControlLabel
              control={<Switch color="primary" value="checkedA" checked={mapOptions.isAnimated} />}
              label="Animations"
              onChange={this.handleAnimationsChange}
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={<Switch color="primary" value="checkedA" checked={mapOptions.isCapitalsOnlyEnabled} />}
              label="Capitals Only"
              onChange={this.handleCapitalsOnlyChange}
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={<Switch color="primary" value="checkedA" checked={mapOptions.isGroupByCityEnabled} />}
              label="Group By City"
              onChange={this.handleGroupByCityChange}
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={<Switch color="primary" value="checkedA" checked={mapOptions.isIsochroneMapEnabled} disabled />}
              label="Isohrone Map"
              onChange={this.handleIsochroneMapChange}
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              disabled={mapOptions.isIsochroneMapEnabled}
              control={<Switch color="primary" value="checkedA" checked={mapOptions.isCategorizeCountriesEnabled} />}
              label="Categorize Country"
              onChange={this.handleCategorizeCountriesChange}
            />
          </MenuItem>
          <MenuItem>
            <RadioGroup
              style={{ flexDirection: 'row' }}
              value={mapOptions.showLinesAs}
              onChange={this.handleShowLinesAsChange}
            >
              <FormControlLabel
                value="trips"
                control={<Radio />}
                label="Trips"
              />
              <FormControlLabel
                value="flights"
                control={<Radio />}
                label="Flights"
              />
              <FormControlLabel
                value="both"
                control={<Radio />}
                label="Both"
              />
            </RadioGroup>
          </MenuItem>
          <MenuItem>
            <RadioGroup
              style={{ flexDirection: 'row' }}
              value={mapOptions.categorizeCountriesAttribute}
              onChange={this.handleCategorizeCountryAttributeChange}
            >
              <FormControlLabel
                value="price"
                disabled={!mapOptions.isCategorizeCountriesEnabled || mapOptions.isIsochroneMapEnabled}
                control={<Radio />}
                label="Price"
              />
              <FormControlLabel
                value="duration"
                disabled={!mapOptions.isCategorizeCountriesEnabled || mapOptions.isIsochroneMapEnabled}
                control={<Radio />}
                label="Duration"
              />
              <FormControlLabel
                value="connections"
                disabled={!mapOptions.isCategorizeCountriesEnabled || mapOptions.isIsochroneMapEnabled}
                control={<Radio />}
                label="Connections"
              />
            </RadioGroup>
          </MenuItem>
          <MenuItem>
            Connections ({mapOptions.connectionsRange.join('-')}):&nbsp;&nbsp;&nbsp;
            <Range min={1} max={10} defaultValue={mapOptions.connectionsRange} onChange={this.handleConnectionsRangeChange} />
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(LongMenu));
