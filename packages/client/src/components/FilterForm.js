import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import TextField from '@material-ui/core/TextField';

import Loading from './Loading';

import {
  setFilterSubscription,
  setFilterOrigin,
  setFilterDate,
} from '../actions/filters';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const selectSubscriptions = ({ subscriptions }) => subscriptions.isReady && !subscriptions.error
  ? subscriptions.data
  : [];

const mapStateToProps = (state) => {
  console.log(state.filters)
  return {
    subscriptions: selectSubscriptions(state),
    filters: state.filters,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setFilterSubscription,
    setFilterOrigin,
    setFilterDate,
  };
};

class NativeSelects extends React.Component {
  handleSubscriptionChange = (e, value) => {
    this.props.setFilterSubscription({ value });
  }

  handleOriginChange = (e, value) => {
    this.props.setFilterOrigin({ value });
  }

  handleDateChange = (e, value) => {
    this.props.setFilterDate({ value });
  }

  render () {
    const {
      classes,
      subscriptions,
      filters,
    } = this.props;

    const selectedSubscr = subscriptions.find(s => +s.id === +filters.subscriptionId);
    const originList = selectedSubscr
      ? Array.isArray(selectedSubscr.airports_list)
        ? selectedSubscr.airports_list
        : selectedSubscr.airports_list.split(',')
      : [];

    return (
      <div className={classes.root}>
        {subscriptions.length && <FormControl fullWidth className={classes.formControl}>
          <InputLabel htmlFor="form-filter-subscription">Subscription</InputLabel>
          <NativeSelect
            value={filters.subscriptionId}
            onChange={this.handleSubscriptionChange}
            input={<Input name="subscription" id="form-filter-subscription" />}
          >
            {subscriptions.map((s, idx) => (
              <option key={s.id} value={s.id}>
                {s.name}
              </option>
            ))}
          </NativeSelect>
          <FormHelperText>Predefined search parameters</FormHelperText>
        </FormControl>}
        {originList.length === 0 ? <Loading /> : <FormControl fullWidth className={classes.formControl}>
          <InputLabel htmlFor="form-filter-origin">Origin</InputLabel>
          <NativeSelect
            value={filters.origin}
            onChange={this.handleOriginChange}
            input={<Input name="origin" id="form-filter-origin" />}
          >
            {originList.map((o) => (
              <option key={o} value={o}>
                {o}
              </option>
            ))}
          </NativeSelect>
          <FormHelperText>Origin of the searches</FormHelperText>
        </FormControl>}
        <FormControl fullWidth className={classes.formControl}>
          <TextField
            id="form-filter-date"
            label="Flying at"
            type="date"
            value={filters.date}
            onChange={this.handleDateChange}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <FormHelperText>Select date to check for trips.</FormHelperText>
        </FormControl>
      </div>
    );
  }
}

NativeSelects.propTypes = {
  classes: PropTypes.object.isRequired,
  subscriptions: PropTypes.array.isRequired,
  filters: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps,
)(NativeSelects));
