import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';

import Trips from './Trips';
import FilterForm from './FilterForm';

const Sidebar = ({ open, onClose, classes }) => console.log(classes) || (
  <Drawer
    variant="persistent"
    anchor="left"
    open={open}
  >
    <div className={classes.paper}>
      <IconButton onClick={(e) => onClose(e)}>
        <ChevronLeftIcon />
      </IconButton>
    </div>
    <FilterForm />
    <Trips />
  </Drawer>
);

Sidebar.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);
