import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Loading from './Loading';
import TripListItem from './TripListItem';

const styles = theme => ({
  root: {
    width: '100%',
    display: 'block',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '40%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  verticalFlex: {
    '& > *': {
      flexDirection: 'column',
    },
  },
  verticalFlex2: {
    padding: 0,
    flexDirection: 'column',
  },
  subheader: {
    display: 'flex',
    alignContent: 'stretch',
    flexFlow: 'row nowrap',
    justifyContent: '',
  },
  bookBtn: {
    right: theme.spacing.unit * 6,
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
  },
});

class TripsList extends React.Component {
  handleClick (event, trip) {
    this.focus
  }

  render () {
    const { classes, trips } = this.props;
    const tripItems = [];

    for (const trip of trips.values()) {
      if (tripItems.length > 3) continue;
      tripItems.push(<TripListItem key={trip.id} trip={trip} />);
    }

    return (
      <div className={classes.root}>
        <Loading />
        {tripItems}
      </div>
    );
  }
}

TripsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TripsList);
