import React from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import Divided from './Divided';
import FlightDetailsListItem from './FlightDetailsListItem';

const FlightDetails = ({ flight }) => (
  <div>
    <List>
      <Divided divider={<Divider />}>
        {[...(new Set([...flight.trips, ...flight.connectionTrips]))].map((trip, idx) => (
          <FlightDetailsListItem key={Math.random()} flight={flight} trip={trip} />
        ))}
      </Divided>
    </List>
  </div>
);

FlightDetails.propTypes = {
  flight: PropTypes.object.isRequired,
};

export default FlightDetails;
