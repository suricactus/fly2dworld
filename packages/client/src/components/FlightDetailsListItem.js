import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { focusTrip } from '../actions/trips';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import SummaryLabels from './SummaryLabels';
import LabelAirline from './labels/LabelAirline';
import LabelDeparts from './labels/LabelDeparts';
import LabelArrives from './labels/LabelArrives';
import LabelChanges from './labels/LabelChanges';
import LabelDistance from './labels/LabelDistance';
import TitleOriginDestination from './labels/TitleOriginDestination';

const mapStateToProps = (state, props) => {
  const changeIdx = 0;

  return {
    changeIdx,
  };
};

const mapDispatchToProps = {
  focusTrip,
};

class FlightDetailsListItem extends Component {
  handleClick (event, tripId) {
    // this.props.focusTrip({ tripId });
  }

  render () {
    const { trip, flight, changeIdx = 0 } = this.props;
    const totalDistance = 666;
    const connection = trip.connections[ changeIdx ];
    const title = <TitleOriginDestination from={trip.airport_from} to={trip.airport_to} />;
    const body = (
      <div>
        <SummaryLabels>
          <LabelDistance distance={flight.distance} total={totalDistance} />
          <LabelChanges count={changeIdx} total={trip.changesCount} />
          <LabelDeparts timestamp={connection.departs_at} />
          <LabelArrives timestamp={connection.arrives_at} />
        </SummaryLabels>
        <LabelAirline code={connection.airline} flightNumber={connection.flight_number.toString()} />
      </div>
    );

    return (
      <ListItem style={{ paddingLeft: 0, paddingRight: 0, }}>
        <ListItemText
          primary={title}
          secondary={body}
          onClick={(event) => this.handleClick(event, trip.id)}
        />
      </ListItem>
    );
  }
}

export default FlightDetailsListItem;
// export default connect(mapStateToProps, mapDispatchToProps)(FlightDetailsListItem);
