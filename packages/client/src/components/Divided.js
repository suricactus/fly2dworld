import React from 'react';
import PropTypes from 'prop-types';

const Divided = ({ children, divider }) => {
  return children.reduce((d, child, idx) => {
    return [
      ...d,
      ...(idx === 0
        ? [child]
        : [
          React.cloneElement(divider, { key: idx }),
          child,
        ]
      ),
    ];
  }, []);
};

Divided.propTypes = {
  children: PropTypes.array.isRequired,
  divider: PropTypes.object.isRequired,
};

export default Divided;
