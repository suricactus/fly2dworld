import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TripsList from './TripsList';

import { setFilterSearchAttribute } from '../actions/filters';

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
  },
});

const selectTrips = ({ trips }) => {
  return trips.trips;
};

const mapStateToProps = (state) => {
  return {
    filters: state.filters,
    trips: selectTrips(state),
  };
};

const mapDispatchToProps = {
  setFilterSearchAttribute,
};

const TabContainer = ({ children }) => {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class FullWidthTabs extends React.Component {
  handleChange = (event, valueIdx) => {
    const { filters } = this.props;
    const searchAttribute = filters.searchAttributesList[ valueIdx ].name;

    this.props.setFilterSearchAttribute({ value: searchAttribute });
  };

  render () {
    const { classes, filters, trips } = this.props;
    const tabButtons = filters.searchAttributesList.map((r, idx) => (
      <Tab key={r.title} value={idx} label={r.title} />
    ));
    const tabBodies = filters.searchAttributesList.map((r, idx) => (
      <TabContainer key={r.title} value={idx}>
        <TripsList trips={trips} />
      </TabContainer>
    ));
    const searchAttributeIdx = filters.searchAttributesList
      .findIndex(r => r.name === filters.searchAttribute);

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={searchAttributeIdx}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
          >
            {tabButtons}
          </Tabs>
        </AppBar>
        <SwipeableViews
          index={searchAttributeIdx}
        >
          {tabBodies}
        </SwipeableViews>
      </div>
    );
  }
}

FullWidthTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  setFilterSearchAttribute: PropTypes.func.isRequired,
  trips: PropTypes.array,
};

export default withStyles(styles, { withTheme: true })(connect(
  mapStateToProps,
  mapDispatchToProps,
)(FullWidthTabs));
