import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import LabelAirport from './LabelAirport';

const TitleOriginDestination = ({ from, to, variant = 'subheading' }) => (
  <Typography variant={variant}>
    <LabelAirport code={from} />
    &nbsp;–&nbsp;
    <LabelAirport code={to} />
  </Typography>
);

TitleOriginDestination.propTypes = {
  variant: PropTypes.string,
  from: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default TitleOriginDestination;
