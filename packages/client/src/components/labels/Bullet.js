import React from 'react';

const Bullet = () => <span style={{ marginTop: '.5rem' }}>•</span>;

export default Bullet;
