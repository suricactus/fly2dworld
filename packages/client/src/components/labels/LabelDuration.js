import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';

momentDurationFormatSetup(moment);

const LabelDuration = ({ duration }) => (
  <span>
    <HourglassEmptyIcon />
    {moment.duration(duration, 'minutes').format('hh[h] mm[m]')}
  </span>
);

LabelDuration.propTypes = {
  duration: PropTypes.number.isRequired,
};

export default LabelDuration;
