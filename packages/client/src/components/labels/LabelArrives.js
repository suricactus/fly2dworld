import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import FlightLandIcon from '@material-ui/icons/FlightLand';

momentDurationFormatSetup(moment);

const LabelArrives = ({ timestamp }) => {
  const ts = moment(timestamp).format('HH:mm');

  return (
    <span>
      <FlightLandIcon />
      {ts}
    </span>
  );
};

LabelArrives.propTypes = {
  timestamp: PropTypes.string.isRequired,
};

export default LabelArrives;
