import React from 'react';
import PropTypes from 'prop-types';
import FlightIcon from '@material-ui/icons/Flight';

const __airlines = {
  BA: {
    name: 'Bulgaria Air',
  },
};

const LabelAirline = ({ code, flightNumber }) => {
  const airline = __airlines[code] || __airlines['BA'];

  return (
    <span>
      <FlightIcon />
      {airline.name} {flightNumber && `#${flightNumber}`}
    </span>
  );
};

LabelAirline.propTypes = {
  code: PropTypes.string.isRequired,
  flightNumber: PropTypes.string,
};

export default LabelAirline;
