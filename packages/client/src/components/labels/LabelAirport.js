import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// const mapStateToProps = ({ airports }) => ({ airports: airports.data || {} });

// const LabelAirport = ({ airports, code }) => {
const LabelAirport = ({ code }) => {
  const airport = window.airports[ code ];

  return (
    <span>
      {airport.city_name}, {airport.country_code} ({code})
    </span>
  );
};

LabelAirport.propTypes = {
  code: PropTypes.string.isRequired,
  // airports: PropTypes.object.isRequired,
};

// export default connect(mapStateToProps)(LabelAirport);
export default LabelAirport;
