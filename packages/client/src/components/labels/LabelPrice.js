import React from 'react';
import PropTypes from 'prop-types';

import EuroSymbolIcon from '@material-ui/icons/EuroSymbol';

const LabelPrice = ({ price }) => {
  return (
    <span>
      <EuroSymbolIcon /> {price}
    </span>
  );
};

LabelPrice.propTypes = {
  price: PropTypes.number.isRequired,
};

export default LabelPrice;
