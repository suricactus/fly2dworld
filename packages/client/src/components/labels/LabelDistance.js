import React from 'react';
import PropTypes from 'prop-types';
import ArrowExpandHorizontal from 'mdi-material-ui/ArrowExpandHorizontal';

const LabelDistance = ({ distance }) => <span><ArrowExpandHorizontal /> {distance} km</span>;

LabelDistance.propTypes = {
  distance: PropTypes.number.isRequired,
};

export default LabelDistance;
