import React from 'react';
import PropTypes from 'prop-types';

import CallMissedOutgoingIcon from '@material-ui/icons/CallMissedOutgoing';

const LabelChanges = ({ count }) => {
  return (
    <span>
      <CallMissedOutgoingIcon /> {count}
    </span>
  );
};

LabelChanges.propTypes = {
  count: PropTypes.number.isRequired,
};

export default LabelChanges;
