import React from 'react';
import PropTypes from 'prop-types';

import RayStartArrow from 'mdi-material-ui/RayStartArrow';

const LabelFlightsForward = ({ flightsCount }) => {
  return (
    <span title="Flights going the opposite direction">
      <RayStartArrow /> {flightsCount}
    </span>
  );
};

LabelFlightsForward.propTypes = {
  flightsCount: PropTypes.number.isRequired,
};

export default LabelFlightsForward;
