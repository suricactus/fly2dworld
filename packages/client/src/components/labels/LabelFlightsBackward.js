import React from 'react';
import PropTypes from 'prop-types';

import RayEndArrow from 'mdi-material-ui/RayEndArrow';

const LabelFlightsBackward = ({ flightsCount }) => {
  return (
    <span title="Flights going the opposite direction">
      <RayEndArrow /> {flightsCount}
    </span>
  );
};

LabelFlightsBackward.propTypes = {
  flightsCount: PropTypes.number.isRequired,
};

export default LabelFlightsBackward;
