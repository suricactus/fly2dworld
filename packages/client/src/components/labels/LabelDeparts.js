import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';

momentDurationFormatSetup(moment);

const LabelDeparts = ({ timestamp }) => {
  const ts = moment(timestamp).format('HH:mm');

  return (
    <span>
      <FlightTakeoffIcon />
      {ts}
    </span>
  );
};

LabelDeparts.propTypes = {
  timestamp: PropTypes.string.isRequired,
};

export default LabelDeparts;
