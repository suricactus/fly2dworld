import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Popup, Polyline } from 'react-leaflet';
import { connect } from 'react-redux';
import AntPath from 'react-leaflet-ant-path';

import TitleOriginDestination from '../labels/TitleOriginDestination';
import LabelDistance from '../labels/LabelDistance';
import LabelDuration from '../labels/LabelDuration';
import LabelFlightsForward from '../labels/LabelFlightsForward';
import LabelFlightsBackward from '../labels/LabelFlightsBackward';
import SummaryLabels from '../SummaryLabels';
import FlightDetails from '../FlightDetails';

class FlightPolyline extends Component {
  shouldComponentUpdate (nextProps) {
    if (nextProps.active !== this.props.active) return true;
    if (nextProps.animate !== this.props.animate) return true;

    return false;
  }

  render () {
    const { animate, flight, onHover, onClick, active } = this.props;
    const PolylineComponent = animate ? AntPath : Polyline;
    const opts = {
      paused: !animate,
      // weight: flight.connections.length,
      weight: 3,
      opacity: active ? 1 : 0.1,
      color: flight.trips.length ? 'red' : 'blue',
    };

    // window.counter = window.counter || 0;
    // console.log(window.counter++)
    // console.log(flight.distance)

    return (
      <PolylineComponent
        key={flight.cacheKey}
        positions={flight.geodesic}
        options={opts}
        color={opts.color}
        opacity={opts.opacity}
        weight={opts.weight}
        onMouseover={e => onHover(e, flight)}
        onMouseout={e => onHover(e, null)}
        onClick={e => onClick(e, flight)}
      >
        <Popup>
          <div>
            <TitleOriginDestination variant="title" from={flight.fromAirport} to={flight.toAirport} />
            <SummaryLabels>
              <LabelDistance distance={flight.distance} />
              <LabelDuration duration={flight.duration || 0} />
              <LabelFlightsForward flightsCount={flight.connections.length - flight.backConnections.length} />
              <LabelFlightsBackward flightsCount={flight.backConnections.length} />
            </SummaryLabels>
            <FlightDetails flight={flight} />
          </div>
        </Popup>
      </PolylineComponent>
    );
  }
}

FlightPolyline.propTypes = {
  flight: PropTypes.object.isRequired,
  animated: PropTypes.bool,
  active: PropTypes.bool,
};

export default FlightPolyline;
