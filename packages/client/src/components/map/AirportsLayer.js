import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Leaflet from 'leaflet';
import { LayerGroup, Marker, Popup } from 'react-leaflet';
import { connect } from 'react-redux';

const mapStateToProps = ({ mapOptions }) => {
  return {
    mapOptions,
  };
};

const mapDispatchToProps = {};

class AirportsLayer extends PureComponent {
  render () {
    const { airports, mapOptions, iconSize } = this.props;
    const airportIcon = Leaflet.icon({
      iconUrl: './airport_icon.png',
      iconSize,
    });

    return (
      <LayerGroup>
        {Object.values(airports).map(airport => {
          return (
            <Marker key={airport.iata_code} position={[airport.lat, airport.lng]} icon={airportIcon}>
              <Popup>
                <div>
                  <h3>{`${airport.name} (${airport.iata_code})`}</h3>
                  <table>
                    <tbody>
                      <tr>
                        <td>Country</td>
                        <td>{airport.countryName} ({airport.countryCode})</td>
                      </tr>
                      <tr>
                        <td>Position</td>
                        <td>{airport.lat}, {airport.lng}</td>
                      </tr>
                      <tr>
                        <td>More info</td>
                        <td>{airport.wikipedia_link}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </Popup>
            </Marker>
          );
        })};
      </LayerGroup>
    );
  }
}

AirportsLayer.propTypes = {
  airports: PropTypes.object.isRequired,
  mapOptions: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AirportsLayer);

