import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { LayerGroup } from 'react-leaflet';
import { connect } from 'react-redux';

import FlightPolyline from './FlightPolyline';

import {
  setHoveredFlight,
  setFocusedFlight,
} from '../../actions/mapCanvas';

const mapStateToProps = ({ mapOptions }) => {
  return {
    mapOptions,
  };
};

const mapDispatchToProps = {};

class FlightsLayer extends Component {
  state = {
    hoveredFlight: null,
  }

  handleFlightMouseover (event, flight) {
    this.setState({ hoveredFlight: flight });
  }

  handleFlightClick (event, flight) {
    console.log('CLICKED')
  }

  isActiveFlight (flight) {
    const { hoveredFlight } = this.state;

    if (!hoveredFlight) return false;

    if (hoveredFlight.cacheKey === flight.cacheKey) return true;

    if (hoveredFlight.tripIds.includes(flight.cacheKey)) return true;

    if (hoveredFlight.flightIds.includes(flight.cacheKey)) return true;

    return false;
  }

  render () {
    const { flights, mapOptions } = this.props;
    const flightRoutes = flights
      // .slice(100, 200)
      .filter(f => {
        if (this.isActiveFlight(f)) return true;

        if (mapOptions.showLinesAs === 'both') return true;

        if (mapOptions.showLinesAs === 'trips' && f.trips.length > 0) return true;

        if (mapOptions.showLinesAs === 'flights' && f.trips.length === 0) return true;

        return false;
      })
      .map(f => {        
        return (
          <FlightPolyline
            key={f.cacheKey}
            flight={f}
            active={this.isActiveFlight(f)}
            onHover={(...args) => this.handleFlightMouseover(...args)}
            onClick={(...args) => this.handleFlightClick(...args)}
          />
        );
      });

    return (
      <LayerGroup>
        {flightRoutes}
      </LayerGroup>
    );
  }
}

FlightsLayer.propTypes = {
  flights: PropTypes.array.isRequired,
  mapOptions: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightsLayer);

