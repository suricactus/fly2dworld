import React, { Component } from 'react';
import Leaflet from 'leaflet';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Map as LeafletMap, TileLayer, GeoJSON } from 'react-leaflet';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import 'leaflet/dist/leaflet.css';
import Flight from '../lib/Flight';
import FlightsLayer from './map/FlightsLayer';
import AirportsLayer from './map/AirportsLayer';

const selectAirports = ({ airports, mapOptions }) => {
  return airports.isReady && !airports.error
    ? Object.values(airports.data)
      .reduce((d, a) => {
        return (!mapOptions.isCapitalsOnly || a.is_capital)
          ? {
            ...d,
            [a.iata_code]: a,
          }
          : d;
      }, {})
    : {};
};

const selectFlights = ({ trips, mapOptions }, airports) => {
  if (!trips.isReady) return [];

  const usage = {};

  for (const trip of trips.data) {
    const cacheKey = `${trip.airport_from}|${trip.airport_to}`;
    const hasCachedFlight = !!usage[ cacheKey ];

    usage[ cacheKey ] = usage[ cacheKey ] || new Flight(trip.airport_from, trip.airport_to);
    usage[ cacheKey ].trips.push(trip);
    usage[ cacheKey ].backConnections = [];

    for (const connection of trip.connections) {
      const cacheKeyInner = `${connection.airport_from}|${connection.airport_to}`;

      usage[ cacheKeyInner ] = usage[ cacheKeyInner ] || new Flight(connection.airport_from, connection.airport_to);
      usage[ cacheKeyInner ].connections.push(connection);
      usage[ cacheKeyInner ].connectionTrips.push(trip);

      usage[ cacheKeyInner ].duration = moment(trip.arrives_at).diff(trip.departs_at);

      usage[ cacheKey ].flightIds.push(cacheKeyInner);
      usage[ cacheKeyInner ].tripIds.push(cacheKey);
    }
  }

  return Object.values(usage);
};

const selectMapCenter = ({ filters }, airports) => {
  const airport = airports[ filters.origin ];

  return airport
    ? [airport.lat, airport.lng]
    : [43, 34];
};

const mapStateToProps = (state) => {
  const airports = selectAirports(state);

  return {
    flights: selectFlights(state, airports),
    mapOptions: state.mapOptions,
    mapCenter: selectMapCenter(state, airports),
    countriesGeojson: state.countriesGeojson,
    airports,
  };
};

const styles = {
};

class MapCanvas extends Component {
  state = {
    zoomLevel: 5,
    airportIconSize: [30, 30],
  }

  handleZoom (event, value) {
    const zoomLevel = event.target.getZoom();
    const possibleIconSizes = [10, 15, 20, 25, 30];
    const MAX_ZOOM_LEVEL = 16;
    const iconSize = possibleIconSizes[ Math.round(zoomLevel / MAX_ZOOM_LEVEL * 10) ];

    this.setState({
      zoomLevel,
      airportIconSize: [iconSize, iconSize],
    });
  }

  render () {
    const { classes, airports, flights, mapOptions, mapCenter, countriesGeojson } = this.props;
    const { zoomLevel, airportIconSize, activeFlight } = this.state;

    return (
      <LeafletMap
        center={mapCenter}
        zoom={zoomLevel}
        style={{height: 'calc(100vh - 80px)'}}
        onZoom={e => this.handleZoom(e)}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors'
        />
        <GeoJSON data={countriesGeojson} />
        <FlightsLayer flights={flights} />
        <AirportsLayer airports={airports} iconSize={airportIconSize} />
      </LeafletMap>
    );
  }
}

MapCanvas.propTypes = {
  flights: PropTypes.array.isRequired,
  airports: PropTypes.object.isRequired,
  mapOptions: PropTypes.object.isRequired,
  mapCenter: PropTypes.array.isRequired,
};

export default withStyles(styles)(connect(
  mapStateToProps,
)(MapCanvas));
