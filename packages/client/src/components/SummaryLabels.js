import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Bullet from './labels/Bullet';
import Divided from './Divided';

const styles = {
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

const SummaryLabels = ({ classes, children }) => (
  <span className={classes.root}>
    <Divided divider={<Bullet />}>
      {children}
    </Divided>
  </span>
);

SummaryLabels.propTypes = {
  children: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SummaryLabels);
