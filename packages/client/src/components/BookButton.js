import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  bookBtn: {
    right: '10px',
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
  },
};

const BookButton = ({ classes, token }) => {
  return (
    <Button
      className={classes.bookBtn}
      mini
      variant="fab"
      color="secondary"
      aria-label="Book"
      href={'https://google.com'}
      target="blank"
    >
      <AddShoppingCartIcon />
    </Button>
  );
};

BookButton.propTypes = {
  classes: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
};

export default withStyles(styles)(BookButton);
