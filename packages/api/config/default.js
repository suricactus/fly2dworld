const config = require('fly2dworld-common').config;

module.exports = {
  ...config,
  auth: {
    accessTokenTtl: 60 * 5,
    refreshTokenTtl: 60 * 1,
  },
  apiPrefix: '',
  port: 2000,

  scaw: {
    prefix: '',
    enabledMiddlewares: [
      'scawDb',
      'passport',
      'cors',
      'slow',
      'morgan',
      'json',
      'ratelimit',
    ],
    $jwt: {
      // cookie: 'AuthorizationCoconfigokie',
      // passthrough: true
    },
    $json: {
      pretty: true,
      param: 'pretty',
    },
    $morgan: 'dev',
    $slow: false,
    $cors: {
      credentials: true,
      allowHeaders: ['Content-Type', 'Authorization'],
      exposeHeaders: ['X-Total-Count', 'Content-Range', 'Content-Type', 'Authorization'],
    },
    $ratelimit: {
      duration: 60000,
      rate: 100000,
      headers: {
        remaining: 'Rate-Limit-Remaining',
        reset: 'Rate-Limit-Reset',
        total: 'Rate-Limit-Total',
      },
      errorMessage: 'Too much request for this period of time',
    },
  },

  middlewares: {
    jwt: {
      // cookie: 'AuthorizationCoconfigokie',
      // passthrough: true
    },
    json: {
      pretty: true,
      param: 'pretty',
    },
    morgan: 'dev',
    slow: false,
    cors: {
      credentials: true,
      allowHeaders: ['Content-Type', 'Authorization'],
      exposeHeaders: ['X-Total-Count', 'Content-Type', 'Authorization'],
    },
    ratelimit: {
      duration: 60000,
      rate: 100000,
      headers: {
        remaining: 'Rate-Limit-Remaining',
        reset: 'Rate-Limit-Reset',
        total: 'Rate-Limit-Total',
      },
      errorMessage: 'Too much request for this period of time',
    },
  },
  jwtSecret: 'hello world',
  crud: {
    select: {
      defaultLimit: 20,
      maxLimit: 100,
    },
  },
  RBAC_RULES: {
    roles: {
      guest: {
        permissions: [
          'GET /',
          'GET /authentication',
          'POST /authentication',
          // TODO move them to admin
          'GET /apiProviders',
          'GET /apiProviders/:id',
          'GET /subscriptions',
          'GET /subscriptions/:id',
        ],
      },
      authenticated: {
        permissions: [
          'GET /countries',
          'GET /countries/:id',
          'GET /predefinedConditions',
          'GET /predefinedConditions/:id',
          'GET /predefinedParams',
          'GET /predefinedParams/:id',
        ],
        inherited: ['guest'],
      },
      admin: {
        permissions: [
          'GET /users',
          'POST /users',
          'DELETE /users/:id',
          'PUT /users/:id',

          'POST /countries',
          'POST /countries/multi',
          'DELETE /countries/:id',
          'DELETE /countries/multi',
          'PUT /countries/:id',
        ],
        inherited: ['guest', 'authenticated'],
      },
    },
    users: {
      'suricactus': ['admin'],
      'editor': ['editor'],
      'guest': ['guest'],
      'admin': ['admin'],
      'authenticated': ['authenticated'],
    },
  },
};
