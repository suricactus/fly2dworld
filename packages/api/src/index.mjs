import Scaw from 'sca-web';

import _root from './_root';
import continents from './continents';
import countries from './countries';
import regions from './regions';
import cities from './cities';
import airlines from './airlines';
import airports from './airports';
import airportTypes from './airportTypes';
import trips from './trips';
import connections from './connections';
import apiProviders from './apiProviders';
import subscriptions from './subscriptions';
import users from './users';
import subscriptionSearches from './subscriptionSearches';

class Fly2dworld extends Scaw {
  bindRoutes () {
    this.router.use('/', _root.routes());
    this.router.use('/airlines', airlines.routes());
    this.router.use('/airports', airports.routes());
    this.router.use('/airportTypes', airportTypes.routes());
    this.router.use('/continents', continents.routes());
    this.router.use('/countries', countries.routes());
    this.router.use('/regions', regions.routes());
    this.router.use('/cities', cities.routes());
    this.router.use('/trips', trips.routes());
    this.router.use('/connections', connections.routes());
    this.router.use('/apiProviders', apiProviders.routes());
    this.router.use('/subscriptions', subscriptions.routes());
    this.router.use('/subscriptionSearches', subscriptionSearches.routes());
    this.router.use('/users', users.routes());
  }
}

const app = new Fly2dworld({});

export default app;
