import crud from 'sca-web/middlewares/crud';
import AirlineModel from 'fly2dworld-common/lib/models/airline';

export default crud(AirlineModel);
