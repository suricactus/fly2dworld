import crud from 'sca-web/middlewares/crud';
import mApiProvider from 'fly2dworld-common/lib/models/apiProvider';

export default crud(mApiProvider);
