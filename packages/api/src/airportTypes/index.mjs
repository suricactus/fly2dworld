import crud from 'sca-web/middlewares/crud';
import AirportTypeModel from 'fly2dworld-common/lib/models/airportType';

export default crud(AirportTypeModel);
