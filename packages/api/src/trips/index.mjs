import crud from 'sca-web/middlewares/crud';
import TripModel from 'fly2dworld-common/lib/models/trip';

export default crud(TripModel);
