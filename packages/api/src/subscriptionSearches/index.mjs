import crud from 'sca-web/middlewares/crud';
import SubscriptionSearchModel from 'fly2dworld-common/lib/models/subscriptionSearch';

export default crud(SubscriptionSearchModel);
