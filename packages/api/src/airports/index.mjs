import crud, { Router } from 'sca-web/middlewares/crud';
import AirportModel from 'fly2dworld-common/lib/models/airport';

const router = new Router();

router.get('/all', async (ctx) => {
  ctx.body = (await ctx.db.query(AirportModel.selectSignificantS()))
    .reduce((d, v) => ({ ...d, [ v.iata_code ]: v }), {});
});

crud(AirportModel, undefined, { router });

export default router;
