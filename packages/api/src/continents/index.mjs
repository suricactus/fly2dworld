import crud from 'sca-web/middlewares/crud';
import ContinentModel from 'fly2dworld-common/lib/models/continent';

export default crud(ContinentModel);
