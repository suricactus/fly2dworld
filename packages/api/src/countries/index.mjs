import crud from 'sca-web/middlewares/crud';
import CountryModel from 'fly2dworld-common/lib/models/country';

export default crud(CountryModel);
