import crud from 'sca-web/middlewares/crud';
import CityModel from 'fly2dworld-common/lib/models/city';

export default crud(CityModel);
