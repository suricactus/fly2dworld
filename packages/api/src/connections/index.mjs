import crud from 'sca-web/middlewares/crud';
import ConnectionModel from 'fly2dworld-common/lib/models/connection';

export default crud(ConnectionModel);
