import crud from 'sca-web/middlewares/crud';
import RegionModel from 'fly2dworld-common/lib/models/region';

export default crud(RegionModel);
