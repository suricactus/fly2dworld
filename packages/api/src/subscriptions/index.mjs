import crud, { Router } from 'sca-web/middlewares/crud';
import SubscriptionModel from 'fly2dworld-common/lib/models/subscription';

const router = new Router();

router.get('/:id/trips', async (ctx) => {
  ctx.body = await ctx.db.query(`select * from trips_vw where search_params_id = 5`);
});

crud(SubscriptionModel, undefined, { router });

export default router;
