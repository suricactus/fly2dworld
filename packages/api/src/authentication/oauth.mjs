import jwt from 'jsonwebtoken';
import config from 'config';
import crypto from 'crypto';
import promisify from 'es6-promisify';

const signAsync = promisify.promisify(jwt.sign, jwt);
const randomBytesAsync = promisify.promisify(crypto.randomBytes, crypto);

const generateJwtId = async () => {
  try {
    let jti = await randomBytesAsync(32);
    return Promise.resolve(jti.toString('hex'));
  } catch (e) {
    return Promise.reject(e);
  }
}

export const generateTokens = async (payload, secret, opts = {}) => {
  try {

    const { auth } = config;

    const accessTokenId = await generateJwtId();
    const refreshTokenId = await generateJwtId();

    const accessTokenPayload = Object.assign({}, payload, { jti: accessTokenId });
    const refreshTokenPayload = Object.assign({}, {
      jti: refreshTokenId,
      ati: accessTokenId
    });

    const refreshTokenOpts = Object.assign({}, {
      expiresIn: auth.refreshTokenTtl
    }, opts);
    const accessTokenOpts = Object.assign({}, {
      expiresIn: auth.accessTokenTtl
    }, opts);

    const refreshToken = await signAsync(refreshTokenPayload, secret, refreshTokenOpts);
    const accessToken = await signAsync(accessTokenPayload, secret, accessTokenOpts);

    return Promise.resolve({
      accessToken,
      refreshToken
    });

  } catch(e) {

    return Promise.reject(e);

  }
};