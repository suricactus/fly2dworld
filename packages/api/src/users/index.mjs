import crud from 'sca-web/middlewares/crud';
import UserModel from 'fly2dworld-common/lib/models/user';

export default crud(UserModel);
