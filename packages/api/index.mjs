import config from 'config';
import app from './src';

if (!global.parent || global.parent.filename.indexOf('api/index.js') !== -1) {
  app.listen(config.get('port'));
  console.info(`API server listening on port ${config.get('port')}`);
  console.info('Press CTRL+C to stop server');
}
