import React from 'react';
import {
  List,
  Edit,
  Datagrid,
  TextField,
  BooleanField,
  EditButton,
  DisabledInput,
  BooleanInput,
  SimpleForm,
  TextInput,
} from 'react-admin';

export const ApiProviderList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="id_code" />
      <TextField source="name" />
      <TextField source="homepage_url" />
      <TextField source="api_url" />
      <TextField source="run_at" />
      <BooleanField source="is_enabled" />
      <EditButton />
    </Datagrid>
  </List>
);

const ApiProviderTitle = ({ record }) => {
  return <span>ApiProvider {record ? `"${record.title}"` : ''}</span>;
};

export const ApiProviderEdit = props => (
  <Edit title={<ApiProviderTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id_code" />
      <TextInput source="name" />
      <TextInput source="homepage_url" />
      <TextInput source="api_url" />
      <TextInput source="api_key" />
      <TextInput source="run_at" />
      <BooleanInput source="is_enabled" />
    </SimpleForm>
  </Edit>
);
