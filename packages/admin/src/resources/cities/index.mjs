import React from 'react';
import {
  List,
  Edit,
  Create,
  Datagrid,
  TextField,
  ReferenceField,
  BooleanField,
  EditButton,
  DisabledInput,
  LongTextInput,
  SimpleForm,
  TextInput,
} from 'react-admin';
import PointField from '../../components/PointField';

export const CityList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ReferenceField label="User" source="country_id" reference="countries">
        <TextField source="name" />
      </ReferenceField>
      <BooleanField source="is_capital" />
      <TextField source="name" />
      <PointField lat="lat" lng="lng" label="Map" />
      <EditButton />
    </Datagrid>
  </List>
);

const CityTitle = ({ record }) => {
  return <span>City {record ? `"${record.title}"` : ''}</span>;
};

export const CityEdit = props => (
  <Edit title={<CityTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const CityCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
