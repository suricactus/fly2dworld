import React from 'react';
import {
  List,
  Edit,
  Create,
  Datagrid,
  TextField,
  BooleanField,
  ReferenceField,
  EditButton,
  DisabledInput,
  LongTextInput,
  SimpleForm,
  TextInput,
} from 'react-admin';
import PointField from '../../components/PointField';
import UrlField from '../../components/UrlField';
import WikipediaField from '../../components/WikipediaField';

export const AirportList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="iata_code" />
      <ReferenceField label="Type" source="type_id" reference="airportTypes">
        <TextField source="name" />
      </ReferenceField>
      <BooleanField source="is_significant" />
      <UrlField source="homepage_url" text="name" />
      <TextField source="municipality" />
      <TextField source="region_id" />
      <WikipediaField source="wikipedia_link" />
      <PointField lat="lat" lng="lng" />
      <EditButton />
    </Datagrid>
  </List>
);

const AirportTitle = ({ record }) => {
  return <span>Airport {record ? `"${record.title}"` : ''}</span>;
};

export const AirportEdit = props => (
  <Edit title={<AirportTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const AirportCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
