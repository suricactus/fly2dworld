import React from 'react';
import { List, Edit, Create, Datagrid, TextField, ReferenceField, EditButton, DisabledInput, LongTextInput, SimpleForm, TextInput } from 'react-admin';

export const RegionList = (props) => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="Country" source="country_id" reference="countries">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="code" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

const RegionTitle = ({ record }) => {
  return (
    <span>Region {record ? `"${record.title}"` : ''}</span>
  );
};

export const RegionEdit = (props) => (
  <Edit title={<RegionTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const RegionCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
