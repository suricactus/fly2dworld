import React from 'react';
import { List, Edit, Create, Datagrid, TextField, NumberField, DateField, ReferenceField, EditButton, DisabledInput, LongTextInput, SimpleForm, TextInput } from 'react-admin';

export const TripList = (props) => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="From" source="airport_from_id" reference="airports">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="To" source="airport_to_id" reference="airports">
        <TextField source="name" />
      </ReferenceField>
      <NumberField source="price" options={{ style: 'currency', currency: 'EUR' }} />
      <DateField source="created_at" />
      <NumberField source="provider_weight" options={{ maximumFractionDigits: 2 }} />
      <NumberField source="connections_count" />
      <NumberField source="time_length" />
      <NumberField source="flight_length" />
      <EditButton />
    </Datagrid>
  </List>
);

const TripTitle = ({ record }) => {
  return (
    <span>Trip {record ? `"${record.title}"` : ''}</span>
  );
};

export const TripEdit = (props) => (
  <Edit title={<TripTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const TripCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
