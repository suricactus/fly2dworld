import React from 'react';
import { List, Edit, Create, Datagrid, TextField, EditButton, SimpleForm, TextInput } from 'react-admin';

export const ContinentList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="code" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

const ContinentTitle = ({ record }) => {
  return (
    <span>Continent {record ? `"${record.title}"` : ''}</span>
  );
};

export const ContinentEdit = (props) => (
  <Edit title={<ContinentTitle />} {...props}>
    <SimpleForm>
      <TextInput source="code" />
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

export const ContinentCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="code" />
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);
