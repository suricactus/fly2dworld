import React from 'react';
import {
  List,
  Edit,
  Create,
  Datagrid,
  TextField,
  NumberField,
  BooleanField,
  EditButton,
  TextInput,
  NumberInput,
  SelectInput,
  BooleanInput,
  SimpleForm,
} from 'react-admin';

export const SortInput = () => (<SelectInput source="sort" isRequired defaultValue={'price'} choices={[
  { id: 'price', name: 'Price' },
  { id: 'convenience', name: 'Convenience' },
  { id: 'duration', name: 'Duration' },
]} />);

export const SubscriptionList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="sort" />
      <BooleanField source="is_enabled" />
      <BooleanField source="is_roundtrip" />
      <BooleanField source="is_direct_flight_only" />
      <NumberField source="adults" />
      <NumberField source="depart_origin_since_now_d" />
      <NumberField source="depart_origin_tolerance_d" />
      <NumberField source="stay_duration_min_d" />
      <NumberField source="stay_duration_max_d" />
      <EditButton />
    </Datagrid>
  </List>
);

const SubscriptionTitle = ({ record }) => {
  return <span>Subscription {record ? `"${record.title}"` : ''}</span>;
};

export const SubscriptionEdit = props => (
  <Edit title={<SubscriptionTitle />} {...props}>
    <SimpleForm>
      <BooleanInput source="is_enabled" defaultValue={true} />
      <BooleanInput source="is_roundtrip" defaultValue={false} />
      <BooleanInput source="is_direct_flight_only" defaultValue={false} />
      <SortInput />
      <NumberInput source="depart_origin_since_now_d" />
      <NumberInput source="depart_origin_tolerance_d" />
      <NumberInput source="return_origin_since_now_d" />
      <NumberInput source="return_origin_tolerance_d" />
      <NumberInput source="stay_duration_min_d" />
      <NumberInput source="stay_duration_max_d" />
      <NumberInput source="adults" />
      <NumberInput source="children" />
      <NumberInput source="infants" />
      <NumberInput source="max_flight_duration_m" />
      <NumberInput source="max_stopovers" />
      <NumberInput source="stopover_min_time_m" />
      <NumberInput source="stopover_max_time_m" />
      <NumberInput source="depart_origin_time_min_m" />
      <NumberInput source="depart_origin_time_max_m" />
      <NumberInput source="arrive_origin_time_min_m" />
      <NumberInput source="arrive_origin_time_max_m" />
      <NumberInput source="depart_return_time_min_m" />
      <NumberInput source="depart_return_time_max_m" />
      <NumberInput source="arrive_return_time_min_m" />
      <NumberInput source="arrive_return_time_max_m" />
      <TextInput source="depart_fly_days" />
      <TextInput source="return_fly_days" />
      <TextInput source="airlines_list" />
      <BooleanInput source="is_airlines_list_exclusion" defaultValue={false} />
      <TextInput source="airports_list" />
      <BooleanInput source="is_airports_list_exclusion" defaultValue={false} />
      <NumberInput source="price_min" />
      <NumberInput source="price_max" />
    </SimpleForm>
  </Edit>
);

export const SubscriptionCreate = props => (
  <Create title={<SubscriptionTitle />} {...props}>
    <SimpleForm>
      <BooleanInput source="is_enabled" defaultValue={true} />
      <BooleanInput source="is_roundtrip" defaultValue={false} />
      <BooleanInput source="is_direct_flight_only" defaultValue={false} />
      <SortInput />
      <NumberInput source="depart_origin_since_now_d" defaultValue={7} />
      <NumberInput source="depart_origin_tolerance_d" defaultValue={0} />
      <NumberInput source="return_origin_since_now_d" />
      <NumberInput source="return_origin_tolerance_d" />
      <NumberInput source="stay_duration_min_d" />
      <NumberInput source="stay_duration_max_d" />
      <NumberInput source="adults" defaultValue={1} />
      <NumberInput source="children" defaultValue={0} />
      <NumberInput source="infants" defaultValue={0} />
      <NumberInput source="max_flight_duration_m" />
      <NumberInput source="max_stopovers" />
      <NumberInput source="stopover_min_time_m" />
      <NumberInput source="stopover_max_time_m" />
      <NumberInput source="depart_origin_time_min_m" />
      <NumberInput source="depart_origin_time_max_m" />
      <NumberInput source="arrive_origin_time_min_m" />
      <NumberInput source="arrive_origin_time_max_m" />
      <NumberInput source="depart_return_time_min_m" />
      <NumberInput source="depart_return_time_max_m" />
      <NumberInput source="arrive_return_time_min_m" />
      <NumberInput source="arrive_return_time_max_m" />
      <TextInput source="depart_fly_days" />
      <TextInput source="return_fly_days" />
      <TextInput source="airlines_list" />
      <BooleanInput source="is_airlines_list_exclusion" defaultValue={false} />
      <TextInput source="airports_list" />
      <BooleanInput source="is_airports_list_exclusion" defaultValue={false} />
      <NumberInput source="price_min" />
      <NumberInput source="price_max" />
    </SimpleForm>
  </Create>
);
