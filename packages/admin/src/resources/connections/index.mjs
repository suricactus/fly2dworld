import React from 'react';
import { List, Edit, Create, Datagrid, TextField, EditButton, DisabledInput, LongTextInput, SimpleForm, TextInput } from 'react-admin';

export const ConnectionList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="api_url" />
      <TextField source="run_at" />
      <EditButton />
    </Datagrid>
  </List>
);

const ConnectionTitle = ({ record }) => {
  return (
    <span>Connection {record ? `"${record.title}"` : ''}</span>
  );
};

export const ConnectionEdit = (props) => (
  <Edit title={<ConnectionTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const ConnectionCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
