import React from 'react';
import { List, Edit, Create, Datagrid, TextField, BooleanField, ReferenceField, EditButton, DisabledInput, LongTextInput, SimpleForm, TextInput } from 'react-admin';
import WikipediaField from '../../components/WikipediaField';

export const CountryList = (props) => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="Continent" source="continent_id" reference="continents">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="code" />
      <TextField source="name" />
      <BooleanField source="is_sovereign" />
      <WikipediaField source="wikipedia_link" />
      <EditButton />
    </Datagrid>
  </List>
);

const CountryTitle = ({ record }) => {
  return (
    <span>Country {record ? `"${record.title}"` : ''}</span>
  );
};

export const CountryEdit = (props) => (
  <Edit title={<CountryTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Edit>
);

export const CountryCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="api_url" />
      <LongTextInput source="run_at" />
    </SimpleForm>
  </Create>
);
