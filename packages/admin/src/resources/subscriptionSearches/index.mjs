import React from 'react';
import {
  List,
  Edit,
  Datagrid,
  TextField,
  NumberField,
  BooleanField,
  EditButton,
  SimpleForm,
} from 'react-admin';

export const SubscriptionSearchList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="search_condition_id" />
      <TextField source="issued_at" />
      <TextField source="sort" />
      <BooleanField source="is_roundtrip" />
      <BooleanField source="is_direct_flight_only" />
      <TextField source="depart_origin_min_date" />
      <TextField source="depart_origin_max_date" />
      <NumberField source="adults" />
      <EditButton />
    </Datagrid>
  </List>
);

const SubscriptionSearchTitle = ({ record }) => {
  return <span>SubscriptionSearch {record ? `"${record.title}"` : ''}</span>;
};

export const SubscriptionSearchEdit = props => (
  <Edit title={<SubscriptionSearchTitle />} {...props}>
    <SimpleForm>
      <TextField source="search_condition_id" />
      <TextField source="issued_at" />
      <TextField source="sort" />
      <BooleanField source="is_roundtrip" />
      <BooleanField source="is_direct_flight_only" />
      <TextField source="depart_origin_min_date" />
      <TextField source="depart_origin_max_date" />
      <TextField source="return_origin_min_date" />
      <TextField source="return_origin_max_date" />
      <NumberField source="stay_duration_min_d" />
      <NumberField source="stay_duration_max_d" />
      <NumberField source="adults" />
      <NumberField source="children" />
      <NumberField source="infants" />
      <NumberField source="max_flight_duration_m" />
      <NumberField source="max_stopovers" />
      <NumberField source="stopover_min_time_m" />
      <NumberField source="stopover_max_time_m" />
      <NumberField source="depart_origin_time_min_m" />
      <NumberField source="depart_origin_time_max_m" />
      <NumberField source="arrive_origin_time_min_m" />
      <NumberField source="arrive_origin_time_max_m" />
      <NumberField source="depart_return_time_min_m" />
      <NumberField source="depart_return_time_max_m" />
      <NumberField source="arrive_return_time_min_m" />
      <NumberField source="arrive_return_time_max_m" />
      <TextField source="depart_fly_days" />
      <TextField source="return_fly_days" />
      <TextField source="airlines_list" />
      <BooleanField source="is_airlines_list_exclusion" />
      <NumberField source="price_min" />
      <NumberField source="price_max" />
    </SimpleForm>
  </Edit>
);
