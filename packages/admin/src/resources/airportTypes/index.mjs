import React from 'react';
import { List, Edit, Create, Datagrid, TextField, EditButton, SimpleForm, TextInput } from 'react-admin';

export const AirportTypeList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

const AirportTypeTitle = ({ record }) => {
  return (
    <span>AirportType {record ? `"${record.title}"` : ''}</span>
  );
};

export const AirportTypeEdit = (props) => (
  <Edit title={<AirportTypeTitle />} {...props}>
    <SimpleForm>
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

export const AirportTypeCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);
