import React from 'react';
import { List, Edit, Create, Datagrid, TextField, BooleanField, EditButton, BooleanInput, DisabledInput, SimpleForm, TextInput } from 'react-admin';

export const AirlineList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="code" />
      <BooleanField source="is_lowcost" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

const AirlineTitle = ({ record }) => {
  return (
    <span>Airline {record ? `"${record.title}"` : ''}</span>
  );
};

export const AirlineEdit = (props) => (
  <Edit title={<AirlineTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="code" />
      <BooleanField source="is_lowcost" />
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

export const AirlineCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="code" />
      <BooleanInput source="is_lowcost" />
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);
