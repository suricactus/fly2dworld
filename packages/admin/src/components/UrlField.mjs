import React from 'react';
import PropTypes from 'prop-types';

const UrlField = ({ source, text, children, target = 'blank', record = {} }) => {
  const attrs = {
    href: record[ source ] || undefined,
  };

  return (
    <a
      target={target}
      {...attrs}
    >
      {children ? children : text ? record[ text ] : record[ source ]}
    </a>
  );
};

UrlField.propTypes = {
  target: PropTypes.string,
  source: PropTypes.string.isRequired,
  text: PropTypes.string,
  record: PropTypes.object,
};

export default UrlField;
