import React from 'react';
import PropTypes from 'prop-types';

const PointField = ({ lat, lng, zoom = 8, text, target = 'blank', record = {} }) => (
  <a
    target={target}
    href={`https://www.openstreetmap.org/#map=${zoom}/${record[lat]}/${record[lng]}&layers=T`}
  >
    {text ? `${text}` : `${record[lat]},${record[lng]}`}
  </a>
);

PointField.propTypes = {
  target: PropTypes.string,
  text: PropTypes.string,
  record: PropTypes.object,
  zoom: PropTypes.number,
  lat: PropTypes.string.isRequired,
  lng: PropTypes.string.isRequired,
};

export default PointField;
