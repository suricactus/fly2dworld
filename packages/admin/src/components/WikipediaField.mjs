import React from 'react';
import PropTypes from 'prop-types';

const WikipediaField = ({ source, target = 'blank', record = {} }) => (
  <a target={target} href={record[source]}>
    Wiki
  </a>
);

WikipediaField.propTypes = {
  target: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
};

export default WikipediaField;
