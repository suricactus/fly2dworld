// in src/App.js
import React from 'react';
import { Admin, Resource } from 'react-admin';

import dataProvider from './dataProvider';
import authProvider from './authProvider';

import { AirlineList, AirlineEdit, AirlineCreate, AirlineIcon } from './resources/airlines';
import { AirportList, AirportEdit, AirportCreate, AirportIcon } from './resources/airports';
import { AirportTypeList, AirportTypeEdit, AirportTypeCreate, AirportTypeIcon } from './resources/airportTypes';
import { ApiProviderList, ApiProviderEdit, ApiProviderIcon } from './resources/apiProviders';
import { CityList, CityEdit, CityCreate, CityIcon } from './resources/cities';
import { ConnectionList, ConnectionEdit, ConnectionCreate, ConnectionIcon } from './resources/connections';
import { ContinentList, ContinentEdit, ContinentCreate, ContinentIcon } from './resources/continents';
import { CountryList, CountryEdit, CountryCreate, CountryIcon } from './resources/countries';
import { RegionList, RegionEdit, RegionCreate, RegionIcon } from './resources/regions';
import { SubscriptionList, SubscriptionEdit, SubscriptionCreate, SubscriptionIcon } from './resources/subscriptions';
import { SubscriptionSearchList, SubscriptionSearchEdit, SubscriptionSearchIcon } from './resources/subscriptionSearches';
import { TripList, TripEdit, TripCreate, TripIcon } from './resources/trips';
import { UserList, UserEdit, UserCreate, UserIcon } from './resources/users';

const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    <Resource name="subscriptions" list={SubscriptionList} edit={SubscriptionEdit} create={SubscriptionCreate} />
    <Resource name="subscriptionSearches" list={SubscriptionSearchList} edit={SubscriptionSearchEdit} />
    <Resource name="trips" list={TripList} edit={TripEdit} create={TripCreate} />
    <Resource name="connections" list={ConnectionList} edit={ConnectionEdit} create={ConnectionCreate} />
    <Resource name="apiProviders" options={{ label: 'API Providers' }} list={ApiProviderList} edit={ApiProviderEdit} />
    <Resource name="users" list={UserList} edit={UserEdit} create={UserCreate} />
    <Resource name="airlines" list={AirlineList} edit={AirlineEdit} create={AirlineCreate} />
    <Resource name="airports" list={AirportList} edit={AirportEdit} create={AirportCreate} />
    <Resource name="airportTypes" list={AirportTypeList} edit={AirportTypeEdit} create={AirportTypeCreate} />
    <Resource name="continents" list={ContinentList} edit={ContinentEdit} create={ContinentCreate} />
    <Resource name="countries" list={CountryList} edit={CountryEdit} create={CountryCreate} />
    <Resource name="regions" list={RegionList} edit={RegionEdit} create={RegionCreate} />
    <Resource name="cities" list={CityList} edit={CityEdit} create={CityCreate} />
  </Admin>
);

export default App;
