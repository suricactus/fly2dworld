import { fetchUtils } from 'react-admin';
// import simpleJsonServerProvider from 'ra-data-json-server';

// const httpClient = (url, options = {}) => {
//   const accessToken = localStorage.getItem('accessToken');

//   if (!options.headers) {
//   	options.headers = new Headers({ Accept: 'application/json' });
//   }

//   options.headers.set('Authorization', `Bearer ${accessToken}`);

//   console.log(url, options)

//   return fetchUtils.fetchJson(url, options);
// }

// export default simpleJsonServerProvider('http://localhost:2000', httpClient);

import simpleRestProvider from 'ra-data-simple-rest';

const httpClient = (url, options = {}) => {
  const accessToken = localStorage.getItem('accessToken');

  if (!options.headers) {
    options.headers = new Headers({ Accept: 'application/json' });
  }

  options.headers.set('Authorization', `Bearer ${accessToken}`);

  console.log(url, options);

  return fetchUtils.fetchJson(url, options);
};

export default simpleRestProvider('http://localhost:2000', httpClient);
