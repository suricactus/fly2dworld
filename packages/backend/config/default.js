const config = require('fly2dworld-common').config;

module.exports = {
  ...config,
  defaultCurrency: 'EUR',
  retryMax: 20,
  retryDelay: 100,
  connectors: {
    KIWI: {
      defaultDateFormat: 'DD/MM/YYYY',
      defaultDurationFormat: 'hh:mm',
      maxQueryLimit: 200,
    },
  },
};
