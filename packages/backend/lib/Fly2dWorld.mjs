import assert from 'assert';
import moment from 'moment';
import subscriptionModel from 'fly2dworld-common/lib/models/subscriptionSearch';
import subscriptionSearchModel from 'fly2dworld-common/lib/models/subscription';
import apiProviderModel from 'fly2dworld-common/lib/models/apiProvider';
import AirportModel from 'fly2dworld-common/lib/models/airport';

const SORTS = ['quality', 'price', 'duration'];

export default class Fly2dWorld {
  constructor ({ ResultsHandler, db, ...settings }) {
    assert(db, 'Missing');

    this.s = settings;
    this.db = db;
    this.resultsHandler = new ResultsHandler({ db });
    this.connectors = new Map();
  }

  get PREDEFINED_PARAMS_COLS () {
    return new Map(
      Object.entries({
        is_roundtrip: d => d.is_roundtrip,
        is_direct_flight_only: d => d.is_direct_flight_only,

        depart_origin_min_date: d => {
          return moment()
            .add({
              days: d.depart_origin_since_now_d,
            })
            .format();
        },
        depart_origin_max_date: d => {
          return moment()
            .add({
              days: d.depart_origin_since_now_d + d.depart_origin_tolerance_d,
            })
            .format();
        },
        return_origin_min_date: d => {
          return d.return_origin_since_now_d === null
            ? null
            : moment()
              .add({
                days: d.return_origin_since_now_d,
              })
              .format();
        },
        return_origin_max_date: d => {
          return d.return_origin_since_now_d === null ||
            d.depart_origin_tolerance_d === null
            ? null
            : moment()
              .add({
                days:
                    d.return_origin_since_now_d + d.depart_origin_tolerance_d,
              })
              .format();
        },
        stay_duration_min_d: d => d.stay_duration_min_d,
        stay_duration_max_d: d => d.stay_duration_min_d,
        adults: d => d.adults,
        children: d => d.children,
        infants: d => d.infants,
        max_flight_duration_m: d => d.max_flight_duration_m,
        max_stopovers: d => d.max_stopovers,
        stopover_min_time_m: d => d.stopover_min_time_m,
        stopover_max_time_m: d => d.stopover_max_time_m,
        depart_origin_time_min_m: d => d.depart_origin_time_min_m,
        depart_origin_time_max_m: d => d.depart_origin_time_max_m,
        arrive_origin_time_min_m: d => d.arrive_origin_time_min_m,
        arrive_origin_time_max_m: d => d.arrive_origin_time_max_m,
        depart_return_time_min_m: d => d.depart_return_time_min_m,
        depart_return_time_max_m: d => d.depart_return_time_max_m,
        arrive_return_time_min_m: d => d.arrive_return_time_min_m,
        arrive_return_time_max_m: d => d.arrive_return_time_max_m,
        price_min: d => d.price_min,
        price_max: d => d.price_max,
        airlines_list: d => d.airlines_list,
        is_airlines_list_exclusion: d => d.is_airlines_list_exclusion,
        airports_list: d => d.airports_list,
        is_airports_list_exclusion: d => d.is_airports_list_exclusion,
      }),
    );
  }

  async getData () {
    const remoteApiConfigList = await this.db.many(apiProviderModel.select({
      filters: {
        is_enabled: true,
      },
    }));

    for (const remoteApiConfig of remoteApiConfigList) {
      const idCode = remoteApiConfig.id_code;

      assert(
        !this.connectors[idCode],
        `Currently supporting only one config for single Connector, but duplicate found for ${idCode}`,
      );
      const Connector = this.s.connectors[idCode];

      assert(Connector instanceof Function, `Missing connector ${idCode}`);

      this.connectors.set(
        idCode,
        new Connector({
          config: remoteApiConfig,
          resultsHandler: this.resultsHandler,
        }),
      );
    }

    const subscriptionSearches = await this.db.any(subscriptionModel.getCurrentlyActive());
    const destinations = (await this.db.many(AirportModel.selectSignificant())).map(d => d.iata_code);

    // Append current timestamp to `search_params` affected rows
    this.db.query(subscriptionModel.appendUpdateTimestamp(subscriptionSearches.map(i => i.id)));

    for (const connector of this.connectors.values()) {
      await connector.search(destinations, subscriptionSearches);
    }
  }

  async generateSearchParams () {
    const searchConditions = await this.db.any(subscriptionSearchModel.selectUnprocessed());
    const predefinedParams = [];
    const now = moment();

    for (const cond of Object.values(searchConditions)) {
      const row = {};

      for (const sortBy of SORTS) {
        for (const [colName, def] of this.PREDEFINED_PARAMS_COLS) {
          if (def === null) continue;

          const val = def(cond);

          assert(
            val !== undefined,
            'Must not have undefined values, but ' + colName,
          );

          row[colName] = val;
        }

        row.issued_at = now.format();
        row.sort = sortBy;
        row.search_condition_id = cond.id;

        predefinedParams.push(row);
      }
    }

    return predefinedParams;
  }

  async savePredefinedSearchParams () {
    const subscriptionSearches = await this.generateSearchParams();

    if (!subscriptionSearches.length) return null;

    const rows = this.db.query(subscriptionModel.batchInsert(subscriptionSearches));

    return rows;
  }
}
