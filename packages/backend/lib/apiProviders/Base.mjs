export default class ApiProvider {
  constructor ({ config, resultsHandler }) {
    this.s = config;
    this.resultsHandler = resultsHandler;
  }

  async saveTrip (trip) {
    await this.resultsHandler.saveTrip(trip);
  }
}
