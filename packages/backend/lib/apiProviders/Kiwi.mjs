import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import config from 'config';
import assert from 'assert';
import querystring from 'querystring';
import chunk from 'lodash/chunk';
import request from 'superagent';
import requestRetry from 'superagent-retry-delay';

import logger from 'fly2dworld-common/lib/logger';

import BaseApiProvider from './Base';

momentDurationFormatSetup(moment);
requestRetry(request);

const isString = val => typeof val === 'string';

export default class Kiwi extends BaseApiProvider {
  constructor (settings) {
    super(settings);

    this.apiVersion = 3;
  }

  get PARAMS_SORT_MAP () {
    return {
      convenience: 'quality',
      price: 'price',
      date: 'date',
      duration: 'duration',
    };
  }

  get PARAMS_MAP () {
    return new Map(
      Object.entries({
        flyFrom: d => d.flyFrom.join(','),
        to: d => d.to.join(','),

        dateFrom: d => this.mapParamsDate(d.depart_origin_min_date),
        dateTo: d => this.mapParamsDate(d.depart_origin_max_date),

        /* IGNORE */
        longitudeFrom: null,
        latitudeFrom: null,
        radiusFrom: null,
        longitudeTo: null,
        latitudeTo: null,
        radiusTo: null,
        /* /IGNORE */

        daysInDestinationFrom: d => d.stay_duration_min_d,
        daysInDestinationTo: d => d.stay_duration_max_d,
        returnFrom: d => this.mapParamsDate(d.return_origin_min_date),
        returnTo: d => this.mapParamsDate(d.return_origin_max_date),
        maxFlyDuration: d => this.mapParamsMinsToHours(d.max_flight_duration_m),
        typeFlight: d => this.mapParamsType(d.is_roundtrip),
        oneforcity: d => true,

        /* IGNORE */
        one_per_date: null,
        passengers: null,
        /* /IGNORE */
        adults: d => d.adults,
        children: d => d.children,
        infants: d => d.infants,
        flyDays: d => this.mapPropsWeeksdays(d.depart_fly_days),

        /* IGNORE */
        flyDaysType: null,
        /* /IGNORE */

        returnFlyDays: d => this.mapPropsWeeksdays(d.return_fly_days),

        /* IGNORE */
        returnFlyDaysType: null,
        onlyWorkingDays: null,
        onlyWeekends: null,
        /* /IGNORE */

        directFlights: d => d.is_direct_flight_only,

        /* IGNORE */
        partner_market: null,
        /* /IGNORE */

        price_from: d => d.price_min,
        price_to: d => d.price_max,
        dtimefrom: d =>
          this.mapParamsMinsToDuration(d.depart_origin_time_min_m),
        dtimeto: d => this.mapParamsMinsToDuration(d.depart_origin_time_max_m),
        atimefrom: d =>
          this.mapParamsMinsToDuration(d.arrive_origin_time_min_m),
        atimeto: d => this.mapParamsMinsToDuration(d.arrive_origin_time_max_m),
        returndtimefrom: d =>
          this.mapParamsMinsToDuration(d.depart_return_time_min_m),
        returndtimeto: d =>
          this.mapParamsMinsToDuration(d.depart_return_time_max_m),
        returnatimefrom: d =>
          this.mapParamsMinsToDuration(d.arrive_return_time_min_m),
        returnatimeto: d =>
          this.mapParamsMinsToDuration(d.arrive_return_time_max_m),
        stopoverfrom: d => this.mapParamsMinsToDuration(d.stopover_min_time_m),
        stopoverto: d => this.mapParamsMinsToDuration(d.stopover_max_time_m),
        maxstopovers: d => d.max_stopovers,

        /* IGNORE */
        connectionsOnDifferentAirport: null,
        returnFromDifferentAirport: null,
        returnToDifferentAirport: null,
        /* /IGNORE */

        selectedAirlines: d => d.airlines_list,
        selectedAirlinesExclude: d =>
          d.airlines_list ? d.is_airlines_list_exclusion : null,
        selectedStopoverAirports: d => d.airports_list,
        selectedStopoverAirportsExclude: d =>
          d.airports_list ? d.is_airports_list_exclusion : null,
        sort: d => this.mapParamsSort(d.sort),
        limit: d => config.get('connectors.KIWI.maxQueryLimit'),
        asc: d => true,
      }),
    );
  }

  makeApiReq (q) {
    const url = this.buildApiUrl(q);

    logger.info('Requesting %s', url);
    logger.debug('Request options', q);

    return request
      .get(url)
      .retry(config.get('retryMax'), config.get('retryDelay'))
      .accept('application/json');
  }

  buildApiUrl (params) {
    if (isString(params)) return params;

    const queryStr = querystring.stringify({
      v: this.apiVersion,
      ...params,
    });

    const url = `${this.s.api_url}/flights?${queryStr}`;

    return url;
  }

  mapParamsMinsToHours (value) {
    return value ? Math.ceil(value / 60) : null;
  }

  mapParamsType (value) {
    return value ? 'round' : 'oneway';
  }

  mapParamsDate (value) {
    if (value === null) return null;

    return moment(value).format(
      config.get('connectors.KIWI.defaultDateFormat'),
    );
  }

  mapParamsMinsToDuration (value) {
    if (value === null) return null;

    const duration = moment.duration({
      minutes: value,
    });

    return duration.format(config.get('connectors.KIWI.defaultDurationFormat'));
  }

  // TODO receive comma separated list of ISO weekdays 1,3,4
  mapPropsWeeksdays (value) {
    return null;
    // return value;
  }

  mapParamsSort (value) {
    assert(
      this.PARAMS_SORT_MAP[value] !== undefined,
      'Unsupported sort ' + value,
    );

    return this.PARAMS_SORT_MAP[value];
  }

  mapParams (subscriptionSearch) {
    const result = {};

    for (const [paramName, paramConfig] of this.PARAMS_MAP) {
      if (paramConfig === null) continue;

      const val =
        paramConfig instanceof Function
          ? paramConfig(subscriptionSearch)
          : subscriptionSearch[paramConfig];

      assert(val !== undefined, 'Undefined value detected for ' + paramName);

      result[paramName] = val;
    }

    return result;
  }

  prepareParams (params) {
    const result = {};

    for (const [paramName, paramVal] of Object.entries(params)) {
      if (paramVal === null) continue;

      const val =
        typeof paramVal === 'boolean'
          ? paramVal
            ? 1
            : 0
          : Array.isArray(paramVal)
            ? JSON.stringify(paramVal)
            : paramVal;

      result[paramName] = val;
    }

    return result;
  }

  async search (destinations, subscriptionSearches) {
    for (const subscriptionSearch of subscriptionSearches) {
      const origins = subscriptionSearch.airports_list
        .split(',')
        .map(o => o.trim());

      for (const origin of origins) {
        let trips;

        try {
          trips = await this.searchSingleOrigin(
            origin,
            destinations,
            subscriptionSearch,
          );
        } catch (e) {
          logger.error(e, `Failed to get `);
        }

        if (trips) {
          try {
            for (const trip of trips) {
              await this.saveTrip(trip);
            }
          } catch (e) {
            logger.error(e, `Failed to save `);
          }
        }
      }
    }
  }

  async searchSingleOrigin (origin, destinations, subscriptionSearch) {
    let responseBodies = [];

    // Sometimes requested destinations are too much, we have to chunkize them
    const destinationChunks =
      destinations.length > 300 ? chunk(destinations, 300) : [destinations];

    subscriptionSearch.flyFrom = [origin];

    // TODO they can be in parallel, but it's not vital right now
    for (const destinationsChunk of destinationChunks) {
      subscriptionSearch.to = destinationsChunk;

      logger.debug({ origin, subscriptionSearch }, 'Kiwi called');

      const params = this.prepareParams(this.mapParams(subscriptionSearch));

      try {
        let apiReqParams = params;

        while (apiReqParams) {
          const resp = await this.makeApiReq(apiReqParams);

          assert(
            resp.body && typeof resp.body === 'object',
            'Missing response body',
          );

          logger.debug({ resp: resp }, 'Response full');

          responseBodies.push(resp.body);

          if (resp.status !== 200) {
            const err = new Error('HTTP status is not 200 OK');
            err.resp = resp;

            throw err;
          }

          if (!resp.body._next) break;

          apiReqParams = resp.body._next;
        }
      } catch (err) {
        logger.error({ err }, 'Failed to get a response:', err.message);
        throw err;
      }
    }

    if (responseBodies.length === 0) { throw new Error('No successufull response', responseBodies); }

    let rawData = [];
    for (const body of responseBodies) {
      rawData = rawData.concat(body.data);
    }

    let parsed;
    try {
      parsed = this.parseRawResp(rawData, subscriptionSearch);
    } catch (err) {
      logger.error({ err }, 'Failed to parse the response:', err.message);

      throw err;
    }

    return parsed;
  }

  parseRawResp (results, params) {
    const rows = [];

    for (let d of results) {
      const row = {};
      let skipRow = false;

      row.apiProviderId = 1;
      row.subscriptionSearchId = params.id;
      row.providerId = d.id;
      row.currencyCode = config.get('defaultCurrency');
      row.price = d.price;
      row.bookingToken = d.booking_token;
      row.providerWeight = d.quality;
      row.airportCodeFrom = d.flyFrom;
      row.airportCodeTo = d.flyTo;

      const connections = [];

      for (let routeRaw of d.route) {
        const isReturn = !!routeRaw.return;

        // TODO sometimes the most optimal way to get to some point is by using train or ferry.
        // this script does not handle this situation, just ignore it!
        if (routeRaw.vehicle_type !== 'aircraft') {
          skipRow = true;
          break;
        }

        const connection = {
          isReturn,
          apiProviderId: row.apiProviderId,
          providerId: routeRaw.id,
          airportCodeFrom: routeRaw.flyFrom,
          airportCodeTo: routeRaw.flyTo,
          flightNumber: routeRaw.flight_no,
          airlineCode: routeRaw.airline,
          prividerId: routeRaw.id,
          departsAt: moment(routeRaw.dTimeUTC * 1000).format(),
          arrivesAt: moment(routeRaw.aTimeUTC * 1000).format(),
        };

        connections.push(connection);
      }

      if (skipRow) continue;

      row.connections = connections;

      rows.push(row);
    }

    logger.debug({ rows }, 'Parsed all rows');

    return rows;
  }
}
