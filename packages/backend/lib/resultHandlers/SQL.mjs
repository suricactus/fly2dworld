import assert from 'assert';

import ResultsBaseHandler from './Base';
import logger from 'fly2dworld-common/lib/logger';

const isNil = val => val === undefined || val === null;
const isNumber = val => typeof val === 'number';
const isBool = val => typeof val === 'boolean';
const quoteIdent = v => `"${v}"`;
const quote = v =>
  isNil(v)
    ? 'NULL'
    : isNumber(v)
      ? v
      : isBool(v)
        ? v
          ? 'TRUE'
          : 'FALSE'
        : `'${v}'`;

export default class ResultsSqlHandler extends ResultsBaseHandler {
  get TRIP_COLS () {
    return {
      api_provider_id: d => d.apiProviderId,
      provider_id: d => d.providerId,
      provider_weight: d => d.providerWeight,
      search_params_id: d => d.subscriptionSearchId,
      airport_from_id: d => this.getAirportId(d.airportCodeFrom),
      airport_to_id: d => this.getAirportId(d.airportCodeTo),
      currency_id: d => this.getCurrencyId(d.currencyCode),
      price: d => d.price,
      booking_token: d => d.bookingToken,
    };
  }

  get TRIP_CONN_COLS () {
    return {
      api_provider_id: d => d.apiProviderId,
      provider_id: d => d.providerId,
      airport_from_id: d => this.getAirportId(d.airportCodeFrom),
      airport_to_id: d => this.getAirportId(d.airportCodeTo),
      airline_id: d => this.getAirlineId(d.airlineCode),
      flight_number: d => d.flightNumber,
      is_return: d => d.isReturn,
      departs_at: d => d.departsAt,
      arrives_at: d => d.arrivesAt,
    };
  }

  connect () {}

  async prepareCols (colDefs, data) {
    const result = {};

    for (const [colName, colDef] of Object.entries(colDefs)) {
      result[colName] = await colDef(data);
    }

    return result;
  }

  quoteHash (hash) {
    const result = new Map();
    const mappedHash =
      hash instanceof Map ? hash : new Map(Object.entries(hash));

    for (const [colName, colValue] of mappedHash) {
      result.set(quoteIdent(colName), quote(colValue));
    }

    return result;
  }

  async saveTrip ({ connections, ...tripData }) {
    const { columnNames, values } = this.stringifyColsVals(
      await this.prepareCols(this.TRIP_COLS, tripData),
      false
    );

    const savedTripConns = await this.db.tx(async t => {
      const savedTrip = await t.oneOrNone(`

        INSERT INTO trips (${columnNames})
        VALUES ${values}
        ON CONFLICT DO NOTHING
        RETURNING *

      `);

      if (!savedTrip) {
        logger.info('Skipping...');
        return;
      }

      logger.info('Saving...');

      const savedConns = await this.saveConn(t, savedTrip, connections);
      const savedTripConns = await this.saveTrippConn(t, savedTrip, savedConns);

      return savedTripConns;
    });

    return savedTripConns;
  }

  async saveConn (db, trip, tripConnsList) {
    const quotedConns = [];
    const connProviderIds = [];
    let apiProviderId;

    for (const conn of tripConnsList) {
      const quotedConn = this.quoteHash(
        await this.prepareCols(this.TRIP_CONN_COLS, conn),
      );

      connProviderIds.push(quote(conn.providerId));
      apiProviderId = quote(conn.apiProviderId);

      quotedConns.push(quotedConn);
    }

    const { columnNames, values } = this.stringifyColsVals(quotedConns);

    await db.none(`

      INSERT INTO connections (${columnNames})
      VALUES ${values}
      ON CONFLICT DO NOTHING

    `);
    const tripConnsRows = db.many(`

      SELECT *
      FROM connections
      WHERE TRUE
        AND api_provider_id = ${apiProviderId}
        AND provider_id IN (${connProviderIds.join(', ')})

      `);

    return tripConnsRows;
  }

  saveTrippConn (db, trip, conns) {
    const tripConns = [];

    for (const conn of conns) {
      const value = new Map(
        Object.entries({
          trip_id: trip.id,
          connection_id: conn.id,
        }),
      );

      tripConns.push(value);
    }

    const { columnNames, values } = this.stringifyColsVals(tripConns, false);

    const result = db.many(`

      INSERT INTO trips_connections (${columnNames})
      VALUES ${values}
      RETURNING *

      `);

    return result;
  }

  getAirportId (airportCode) {
    return this.getNomenclatureId({
      table: 'airports',
      searchCol: 'iata_code',
      searchBy: airportCode,
    });
  }

  getAirlineId (airlineCode) {
    return this.getNomenclatureId({
      table: 'airlines',
      searchCol: 'code',
      searchBy: airlineCode,
    });
  }

  getCurrencyId (currencyCode) {
    return this.getNomenclatureId({
      table: 'currencies',
      searchCol: 'code',
      searchBy: currencyCode,
    });
  }

  stringifyColsVals (data, isQuoted = true, trustArrayData = true) {
    if (!Array.isArray(data)) {
      data = [data];
    }

    assert(
      trustArrayData,
      'Not implemented, check if all array elements are objects with the same structure',
    );

    const quotedData = [];

    for (const [idx, d] of data.entries()) {
      quotedData[idx] = isQuoted ? d : this.quoteHash(d);
    }

    const columnNames = Array.from(quotedData[0].keys()).join(', ');
    const values = quotedData
      .map(d => {
        const quotedRow = Array.from(d.values()).join(', ');
        return `(${quotedRow})`;
      })
      .join(', ');

    assert(columnNames);
    assert(values);

    return {
      columnNames,
      values,
    };
  }

  async getNomenclatureId ({ table, searchCol, searchBy }) {
    assert(table);
    assert(searchCol);
    assert(searchBy);

    const cacheKey = `${table}_${searchBy}`;

    if (this.CACHE[cacheKey] !== undefined) {
      return this.CACHE[cacheKey];
    }

    logger.info(`Currently getting ${cacheKey}`);

    let result = await this.db.one(`

      SELECT *
      FROM ${quoteIdent(table)}
      WHERE ${quoteIdent(searchCol)} = ${quote(searchBy)}

    `);

    this.CACHE[cacheKey] = result.id;

    return this.CACHE[cacheKey];
  }
}
