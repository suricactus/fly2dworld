export default class ResultsBaseHandler {
  constructor ({ db }) {
    this.db = db;
    this.CACHE = {};
  }

  get cacheKey () {
    return 'CACHE_KEY';
  }
}
