import logger from 'fly2dworld-common/lib/logger';
import dbConnect from 'fly2dworld-common/lib/db';

import ApiProviderKiwi from './apiProviders/Kiwi';
import ResultsHandlerSql from './resultHandlers/SQL';

import Fly2dWorld from './Fly2dWorld';

const start = async () => {
  const db = await dbConnect();

  const app = new Fly2dWorld({
    ResultsHandler: ResultsHandlerSql,
    connectors: {
      KIWI: ApiProviderKiwi,
    },
    db: db,
  });

  await app.savePredefinedSearchParams();

  try {
    await app.getData();
    logger.info('Successfully started app');
  } catch (e) {
    logger.info(e, 'Error!');
  } finally {
    logger.info('Exit app');
  }
};

export default () => {
  logger.info('Starting...');

  return start()
    .catch(e => {
      logger.error(e, e.message);
    });
};
