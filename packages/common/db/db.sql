-- CREATE DATABASE fly2dworld;

CREATE EXTENSION postgis;

CREATE TABLE IF NOT EXISTS continents (
  id serial PRIMARY KEY,
  code text NOT NULL UNIQUE,
  name text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS countries (
  id serial PRIMARY KEY,
  continent_id serial NOT NULL REFERENCES continents,
  code text NOT NULL UNIQUE,
  name text NOT NULL,
  is_sovereign boolean NOT NULL,
  wikipedia_link text,
  keywords text
);

CREATE TABLE IF NOT EXISTS regions (
  id serial PRIMARY KEY,
  country_id serial NOT NULL REFERENCES countries,
  code text NOT NULL UNIQUE,
  name text NOT NULL
);

CREATE TABLE IF NOT EXISTS cities (
  id serial PRIMARY KEY,
  country_id serial NOT NULL UNIQUE REFERENCES countries,
  is_capital boolean NOT NULL,
  name text NOT NULL,
  lat real NOT NULL,
  lng real NOT NULL
);

CREATE TABLE IF NOT EXISTS airport_types (
  id serial PRIMARY KEY,
  name text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS airports (
  id serial PRIMARY KEY,
  iata_code text NOT NULL,
  local_code text,
  lat real NOT NULL,
  lng real NOT NULL,
  type_id int NOT NULL REFERENCES airport_types,
  name text NOT NULL,
  elevation text NOT NULL,
  region_id int NOT NULL REFERENCES regions,
  city_id int REFERENCES cities,
  municipality text NOT NULL,
  gps_code text,
  homepage_url text,
  wikipedia_link text,
  keywords text
);

CREATE TABLE IF NOT EXISTS airlines (
  id serial PRIMARY KEY,
  country_id int REFERENCES countries,
  code text NOT NULL UNIQUE,
  name text NOT NULL,
  is_lowcost boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS currencies (
  id serial PRIMARY KEY,
  code text NOT NULL UNIQUE,
  name text NOT NULL
);

INSERT INTO currencies (code, name) VALUES ('EUR', 'Euro');

CREATE TABLE IF NOT EXISTS api_providers  (
  id integer PRIMARY KEY,
  id_code text NOT NULL UNIQUE,
  is_enabled boolean NOT NULL,
  name text NOT NULL UNIQUE,
  homepage_url text NOT NULL UNIQUE,
  api_url text NOT NULL UNIQUE,
  api_key text NOT NULL,
  run_at time NOT NULL
);

INSERT INTO api_providers (id, id_code, is_enabled, name, homepage_url, api_url, api_key, run_at)
VALUES (1, 'KIWI', TRUE, 'kiwi.com', 'https://kiwi.com/', 'https://api.skypicker.com/', 'picky', '02:00');


CREATE TYPE sort_t AS ENUM ('convenience', 'price', 'duration');

CREATE TABLE IF NOT EXISTS subscriptions (
  id serial PRIMARY KEY,
  name text not null UNIQUE,
  is_enabled boolean NOT NULL,
  is_roundtrip boolean NOT NULL,
  is_direct_flight_only boolean NOT NULL,

  depart_origin_since_now_d int NOT NULL,
  depart_origin_tolerance_d int NOT NULL,
  return_origin_since_now_d int,
  return_origin_tolerance_d int,
  stay_duration_min_d int,
  stay_duration_max_d int,

  adults int NOT NULL,
  children int NOT NULL,
  infants int NOT NULL,

  max_flight_duration_m int,
  max_stopovers int,

  stopover_min_time_m int,
  stopover_max_time_m int,

  depart_origin_time_min_m int,
  depart_origin_time_max_m int,
  arrive_origin_time_min_m int,
  arrive_origin_time_max_m int,
  depart_return_time_min_m int,
  depart_return_time_max_m int,
  arrive_return_time_min_m int,
  arrive_return_time_max_m int,

  depart_fly_days text,
  return_fly_days text,
  airlines_list text[],
  is_airlines_list_exclusion boolean NOT NULL DEFAULT FALSE,
  airports_list text not null,

  sort_by_list sort_t[] not null,
  price_min int,
  price_max int
);


INSERT INTO subscriptions (
  is_enabled,
  is_roundtrip,
  is_direct_flight_only,
  name,
  depart_origin_since_now_d,
  depart_origin_tolerance_d,
  return_origin_since_now_d,
  return_origin_tolerance_d,
  stay_duration_min_d,
  stay_duration_max_d,
  adults,
  children,
  infants,
  airports_list,
  sort_by_list
)
VALUES
  (TRUE, FALSE, FALSE, 'Depart after 1 month', 30, 7, 0, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, FALSE, FALSE, 'Depart after 1 month (strict)', 30, 0, 0, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, FALSE, FALSE, 'Depart after 1 week (strict)', 7, 0, 0, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 3 days', 30, 7, 33, 7, 2, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 5 days', 30, 7, 35, 7, 4, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 9 days', 30, 7, 39, 7, 8, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 3 days (strict)', 30, 0, 33, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 5 days (strict)', 30, 0, 35, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 month, stay 9 days (strict)', 30, 0, 39, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 week, stay 3 days', 7, 0, 10, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 week, stay 5 days', 7, 0, 12, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}'),
  (TRUE, TRUE, FALSE, 'Depart after 1 week, stay 9 days', 7, 0, 16, 0, 0, 0, 1, 0, 0, '{SOF,OTP,LHR,SKP,BEG,ATH,SKG,IST}', '{price,duration,convenience}')
;


CREATE TABLE IF NOT EXISTS subscription_searches (
  id serial PRIMARY KEY,
  subscription_id int NOT NULL REFERENCES subscriptions,
  issued_at timestamp NOT NULL,

  sort sort_t NOT NULL,
  is_roundtrip boolean NOT NULL,
  is_direct_flight_only boolean NOT NULL,

  depart_origin_min_date timestamp NOT NULL,
  depart_origin_max_date timestamp NOT NULL,
  return_origin_min_date timestamp,
  return_origin_max_date timestamp,
  stay_duration_min_d int,
  stay_duration_max_d int,

  adults int NOT NULL,
  children int NOT NULL,
  infants int NOT NULL,

  max_flight_duration_m int,
  max_stopovers int,

  stopover_min_time_m int,
  stopover_max_time_m int,

  depart_origin_time_min_m int,
  depart_origin_time_max_m int,
  arrive_origin_time_min_m int,
  arrive_origin_time_max_m int,
  depart_return_time_min_m int,
  depart_return_time_max_m int,
  arrive_return_time_min_m int,
  arrive_return_time_max_m int,

  depart_fly_days text,
  return_fly_days text,
  airlines_list text[],
  is_airlines_list_exclusion boolean NOT NULL,
  airports_list text[],

  price_min int,
  price_max int,

  update_timestamps timestamp[] NOT NULL DEFAULT '{}',

  UNIQUE(subscription_id, issued_at)
);



CREATE TABLE IF NOT EXISTS trips (
  id serial PRIMARY KEY,
  api_provider_id int NOT NULL REFERENCES api_providers,
  subscription_search_id int NOT NULL REFERENCES subscription_searches,
  airport_from_id int NOT NULL REFERENCES airports,
  airport_to_id int NOT NULL REFERENCES airports,
  currency_id int NOT NULL REFERENCES currencies,
  price real NOT NULL,
  created_at timestamp NOT NULL DEFAULT NOW(),
  provider_weight text,
  provider_id text,
  booking_token text NOT NULL,
  UNIQUE(api_provider_id, provider_id)
);

CREATE TABLE IF NOT EXISTS connections (
  id serial PRIMARY KEY,
  api_provider_id int NOT NULL REFERENCES api_providers,
  airport_from_id int NOT NULL REFERENCES airports,
  airport_to_id int NOT NULL REFERENCES airports,
  airline_id int NOT NULL REFERENCES airlines,
  flight_number int NOT NULL,
  departs_at timestamp NOT NULL,
  arrives_at timestamp NOT NULL,
  is_return boolean NOT NULL,
  provider_id text,
  UNIQUE(api_provider_id, provider_id)
);

CREATE TABLE IF NOT EXISTS trips_connections (
  id serial PRIMARY KEY,
  trip_id int NOT NULL REFERENCES trips,
  connection_id int NOT NULL REFERENCES connections,
  UNIQUE (trip_id, connection_id)
);

CREATE TABLE IF NOT EXISTS users (
  id serial PRIMARY KEY,
  login text NOT NULL UNIQUE,
  password text NOT NULL
);

CREATE OR REPLACE VIEW airports_vw AS
SELECT
  a.*,
  ST_MakePoint(a.lng, a.lat) geom,
  coalesce(ci.name, municipality) city_name,
  r.code region_code,
  c.code country_code,
  c.name country_name,
  ci.id IS NOT NULL is_capital
FROM airports a
LEFT JOIN regions r on a.region_id = r.id
LEFT JOIN countries c on r.country_id = c.id
LEFT JOIN cities ci ON a.city_id = ci.id;

CREATE OR REPLACE VIEW cities_vw AS
SELECT *, ST_MakePoint(c.lng, c.lat) geom
FROM cities c
WHERE is_capital = TRUE;

CREATE OR REPLACE VIEW significant_airports_vw AS
SELECT a.*, at.name AS type_name
FROM airports_vw a
JOIN airport_types at ON a.type_id = at.id
WHERE TRUE
  AND at.name ~ 'airport'
  AND (FALSE
    OR city_id IS NOT NULL
    OR at.name = 'large_airport'
    OR a.name ~ 'International'
    OR a.iata_code IN ('TRF', 'NYO', 'IXC', 'BVA', 'PIK', 'RZE', 'CMN', 'ABJ', 'COR', 'NTE', 'PGD')
  );


DROP VIEW IF EXISTS something3_vw;
DROP VIEW IF EXISTS something2_vw;
DROP VIEW IF EXISTS something_vw;

CREATE OR REPLACE VIEW something_vw AS
SELECT
  t.api_provider_id,
  t.subscription_search_id,
  t.airport_from_id,
  t.airport_to_id,
  c.is_return,
  min(t.price) price,
  sum(EXTRACT(EPOCH FROM c.arrives_at - c.departs_at) / 60) flight_time_m,
  min(c.departs_at) departs_at,
  max(c.arrives_at) arrives_at,
  count(*) connections_count,
  jsonb_agg(jsonb_build_object(
    'airport_from_id', c.airport_from_id,
    'airport_to_id', c.airport_to_id,
    'airline_id', c.airline_id,
    'flight_number', c.flight_number,
    'departs_at', c.departs_at,
    'arrives_at', c.arrives_at
  )) connections
FROM trips t
JOIN trips_connections tc ON t.id = tc.trip_id
JOIN connections c ON tc.connection_id = c.id AND t.api_provider_id = c.api_provider_id
GROUP BY
  t.api_provider_id,
  t.subscription_search_id,
  t.airport_from_id,
  t.airport_to_id,
  c.is_return
ORDER BY 1, 2, 3
;

CREATE OR REPLACE VIEW something2_vw AS
SELECT
  a1.city_id city_from_id,
  a2.city_id city_to_id,
  EXTRACT(EPOCH FROM s.arrives_at - s.departs_at) / 60 total_time_m,
  s.*
FROM something_vw s
JOIN airports_vw a1 ON a1.id = s.airport_from_id
JOIN airports_vw a2 ON a2.id = s.airport_TO_id
;

CREATE OR REPLACE VIEW something3_vw AS
SELECT
  s.api_provider_id,
  s.subscription_search_id,
  s.airport_from_id,
  s.airport_to_id,
  s.city_from_id,
  s.city_to_id,
  s.price,
  sum(total_time_m) total_travel_time_m,
  sum(flight_time_m) total_flight_time_m,
  sum(connections_count) total_connections_count,
  jsonb_object_agg(
    CASE s.is_return WHEN TRUE THEN 'return' ELSE 'depart' END,
    jsonb_build_object(
      'total_travel_time', s.total_time_m,
      'connections', connections,
      'connections_count', connections_count
    )
  )
FROM something2_vw s
GROUP BY
  s.api_provider_id,
  s.subscription_search_id,
  s.airport_from_id,
  s.airport_to_id,
  s.city_from_id,
  s.city_to_id,
  s.price
;

DROP VIEW something4_vw;
CREATE OR REPLACE VIEW something4_vw AS
SELECT
  city_to_id,
  city_from_id,
  price
FROM something3_vw
WHERE city_to_id IS NOT NULL
GROUP BY
  city_from_id,
  city_to_id,
  price
HAVING TRUE
  AND price = min(price);

CREATE USER api;
GRANT ALL PRIVILEGES ON ALL TABLES after SCHEMA public TO api;
GRANT ALL PRIVILEGES ON ALL SEQUENCES after SCHEMA public TO api;


-- добавено е DME, Русия
-- премахнато е AUT, Източен Тимор
-- премахнато е DGG, Папуа-Нова Гвинея
-- премахнато е MBU, Соломонови острови
-- премахнато е UIQ, Вануату
-- премахнато е IMI, Маршалови острови
-- премахнато е EUA, Тонга
-- премахнато е MOZ, Таити
-- премахнато е MQS, Сейнт Винсент и Гренадини
-- премахнато е BQU, Сейнт Винсент и Гренадини
-- премахнато е VIJ, Британски Вирджински острови
-- премахнато е DDP, Пуерто Рико
-- премахнато е STT, Пуерто Рико
-- премахнато е MZE, Белиз
-- премахнато е SQS, Белиз
-- премахнато е CYD, Белиз
-- премахнато е POT, Ямайка













-- WITH RECURSIVE sth(trip_id, airports) AS (
--   SELECT c.trip_id, ARRAY[c.airport_from, c.airport_to] FROM connections_1 c
--   UNION ALL
--   SELECT c.trip_id, s.airports || c.airport_to
--   FROM connections_1 c, sth s
--   WHERE s.airports[1] = c.airport_from
-- )
-- SELECT *
-- FROM sth
-- LIMIT 100;


-- CREATE VIEW connections_1 AS   
-- SELECT
--     tc.trip_id trip_id,
--     a1.iata_code airport_from,
--     a2.iata_code airport_to
--   FROM connections c
--   JOIN airports a1 ON a1.id = c.airport_from_id
--   JOIN airports a2 ON a2.id = c.airport_to_id
--   JOIN trips_connections tc ON c.id =tc.connection_id



CREATE OR REPLACE VIEW trips_vw AS
  WITH connections_agg AS (
    SELECT tc.trip_id,
      array_agg(jsonb_build_object(
          'id', c_1.id,
          'airport_from', a1_1.iata_code,
          'airport_to', a2_1.iata_code,
          'departs_at', c_1.departs_at,
          'arrives_at', c_1.arrives_at,
          'flight_number', c_1.flight_number,
          'airline', airlines.code
      )) AS airports
     FROM connections c_1
       JOIN airports a1_1 ON a1_1.id = c_1.airport_from_id
       JOIN airports a2_1 ON a2_1.id = c_1.airport_to_id
       JOIN trips_connections tc ON c_1.id = tc.connection_id
       JOIN airlines ON airlines.id = c_1.airline_id
    GROUP BY tc.trip_id
  )

 SELECT t.id,
    a1.iata_code AS airport_from,
    a2.iata_code AS airport_to,
    t.price,
    currencies.code AS currency,
    c.airports AS connections,
    t.booking_token,
    t.search_params_id
   FROM trips t
     JOIN airports a1 ON a1.id = t.airport_from_id
     JOIN airports a2 ON a2.id = t.airport_to_id
     JOIN connections_agg c ON t.id = c.trip_id
     JOIN currencies ON currencies.id = t.currency_id;

