COPY public.airlines (id, country_id, code, name, is_lowcost) FROM stdin;
1	\N	Y4	Volaris	t
2	\N	2N	NextJet	t
3	\N	SE	XL Airways France	t
4	\N	QD	JC International Airlines	f
5	\N	X4	Alaska Seaplanes X4	f
6	\N	JU	Air Serbia	f
7	\N	V0	Conviasa	t
8	\N	AG	Aruba Airlines	f
9	\N	UJ	AlMasria Universal Airlines	f
10	\N	2A	Deutsche Bahn	f
11	\N	LP	LATAM Peru	f
12	\N	W3	Arik Air	f
13	\N	JI	Meraj Air	f
14	\N	DI	Norwegian Air UK	f
15	\N	VE	EasyFly	f
16	\N	NY	Air Iceland Connect	t
17	\N	ET	Ethiopian Airlines	f
18	\N	5T	Canadian North	f
19	\N	F3	Flyadeal	f
20	\N	NT	Binter Canarias	t
21	\N	CY	Cyprus Airways	f
22	\N	6I	Alsie Express	f
23	\N	PR	Philippine Airlines	f
24	\N	2P	PAL Express	t
25	\N	9J	Dana Airlines Limited	f
26	\N	SX	SkyWork Airlines	f
27	\N	8G	Mid Africa Aviation	f
28	\N	SF	Tassili Airlines	f
29	\N	RT	JSC UVT Aero	f
30	\N	BJ	NouvelAir	f
31	\N	MV	Air Mediterranean	f
32	\N	LH	Lufthansa	f
33	\N	D4	Aerodart	f
34	\N	OV	SalamAir	t
35	\N	LA	LATAM Chile	f
36	\N	LQ	Lanmei Airlines	t
37	\N	PE	People's Viennaline PE	f
38	\N	S9	Starbow Airlines	f
39	\N	9P	Air Arabia Jordan	t
40	\N	SV	Saudi Arabian Airlines	f
41	\N	KS	Peninsula Airways	f
42	\N	6D	Pelita	f
43	\N	99	Ciao Air	f
44	\N	BA	British Airways	f
45	\N	J2	Azerbaijan Airlines	f
46	\N	Z7	Amaszonas Uruguay	t
47	\N	ZP	Amaszonas del Paraguay S.A. Lineas Aereas	f
48	\N	VT	Air Tahiti	f
49	\N	EK	Emirates	f
50	\N	XW	NokScoot	t
51	\N	WE	Thai Smile	t
52	\N	A1	Atifly	f
53	\N	VL	Med-View Airline	f
54	\N	9U	Air Moldova	f
55	\N	HM	Air Seychelles	f
56	\N	LI	Leeward Islands Air Transport	f
57	\N	TQ	Tandem Aero	f
58	\N	9I	Thai Sky Airlines	f
59	\N	XQ	SunExpress	t
60	\N	L7	Lugansk Airlines	f
61	\N	UG	TunisAir Express	f
62	\N	T0	TACA Peru	f
63	\N	J5	Alaska Seaplane Service	f
64	\N	E2	Eagle Atlantic Airlines	f
65	\N	NP	Nile Air	f
66	\N	CD	Corendon Dutch Airlines B.V.	f
67	\N	WF	Widerøe	f
68	\N	3I	Air Comet Chile	f
69	\N	HN	Hankook Airline	f
70	\N	II	LSM International 	f
71	\N	W7	Austral Brasil	f
72	\N	FL	AirTran Airways	f
73	\N	OO	SkyWest	f
74	\N	ZM	Air Manas	f
75	\N	0	Anadolujet	t
76	\N	S1	Serbian Airlines	f
77	\N	MI	SilkAir	f
78	\N	B4	ZanAir	f
79	\N	HG	Niki	f
80	\N	M2	MHS Aviation GmbH	f
81	\N	M1	Maryland Air	f
82	\N	LT	Air Lituanica	f
83	\N	TE	FlyLal	f
84	\N	11	TUIfly (X3)	f
85	\N	7C	Jeju Air	t
86	\N	AY	Finnair	f
87	\N	AV	Avianca	f
88	\N	Y7	NordStar Airlines	t
89	\N	PM	Canary Fly	f
90	\N	RJ	Royal Jordanian	f
91	\N	KK	AtlasGlobal	t
92	\N	7F	First Air	f
93	\N	A7	Air Plus Comet	f
94	\N	ZH	Shenzhen Airlines	f
95	\N	QR	Qatar Airways	f
96	\N	GG	Air Guyane	f
97	\N	2U	Air Guinee Express	f
98	\N	IN	Nam Air	f
99	\N	4C	LATAM Colombia	f
100	\N	FK	Africa West	f
101	\N	FZ	Fly Dubai	t
102	\N	4D	Air Sinai	f
103	\N	XM	Alitalia Express	f
104	\N	YW	Air Nostrum	f
105	\N	6S	SaudiGulf Airlines	f
106	\N	LG	Luxair	f
107	\N	N7	Neptune Air Sdn Bhd	f
108	\N	ZT	Titan Airways	f
109	\N	QT	TAMPA	f
110	\N	4Z	Airlink (SAA)	f
111	\N	VD	Air Libert	f
112	\N	ZW	Air Wisconsin	f
113	\N	HP	America West Airlines	f
114	\N	4A	Air Kiribati	f
115	\N	SQ	Singapore Airlines	f
116	\N	SZ	Somon Air	f
117	\N	NN	VIM Airlines	t
118	\N	7P	AirPanama	t
119	\N	5D	Aerolitoral	f
120	\N	SB	Aircalin	f
121	\N	2O	Air Salone	f
122	\N	QQ	Alliance Airlines	f
123	\N	I9	Air Italy	f
124	\N	WY	Oman Air	f
125	\N	U6	Ural Airlines	t
126	\N	K2	EuroLot	f
127	\N	4T	Belair	f
128	\N	AP	AlbaStar	f
129	\N	QH	Air Kyrgyzstan	f
130	\N	KO	Komiaviatrans	f
131	\N	WI	White Airways	f
132	\N	EI	Aer Lingus	t
133	\N	QF	Qantas	f
134	\N	KJ	British Mediterranean Airways	f
135	\N	I8	Izhavia	f
136	\N	GK	Jetstar Japan	f
137	\N	GL	Air Greenland	f
138	\N	2I	Star Peru	t
139	\N	A5	HOP!	t
140	\N	PX	Air Niugini	f
141	\N	UI	Auric Air	f
142	\N	H1	Hahn Air	f
143	\N	E5	Air Arabia Egypt	f
144	\N	HA	Hawaiian Airlines	f
145	\N	IQ	Qazaq Air	f
146	\N	WZ	Red Wings	t
147	\N	BN	Horizon Airlines	f
148	\N	W8	Cargojet Airways	f
149	\N	PT	Red Jet Andes	f
150	\N	6E	IndiGo Airlines	t
151	\N	PP	Air Indus	f
152	\N	TC	Air Tanzania	f
153	\N	PJ	Air Saint Pierre	f
154	\N	Z4	Zoom Airlines	f
155	\N	JY	interCaribbean Airways	f
156	\N	JM	Air Jamaica	f
157	\N	GQ	Sky Express	f
158	\N	NE	Nesma Air	f
159	\N	DF	Michael Airlines	f
160	\N	U1	Aviabus	f
161	\N	0X	Copenhagen Express	f
162	\N	CT	Alitalia Cityliner	f
163	\N	AC	Air Canada	f
164	\N	8Q	Onur Air	t
165	\N	LR	LACSA	f
166	\N	B9	Air Bangladesh	f
167	\N	7E	Aeroline GmbH	f
168	\N	6G	Air Wales	f
169	\N	LU	LAN Express	f
170	\N	5Y	Atlas Air	f
171	\N	R7	Aserca Airlines	f
172	\N	AS	Alaska Airlines	f
173	\N	VU	Air Ivoire	f
174	\N	WX	CityJet	f
175	\N	IT	Tigerair Taiwan	f
176	\N	EH	ANA Wings	f
177	\N	0B	Blue Air	t
178	\N	ZL	Regional Express	t
179	\N	3E	Air Choice One	f
180	\N	FN	Fastjet	t
181	\N	CV	Air Chathams	f
182	\N	MU	China Eastern Airlines	f
183	\N	SY	Sun Country Airlines	t
184	\N	MD	Air Madagascar	f
185	\N	U8	TUS Airways	f
186	\N	FV	Rossiya-Russian Airlines	f
187	\N	D9	Aeroflot-Don	f
188	\N	NM	Air Madrid	f
189	\N	OF	Air Finland	f
190	\N	W9	Air Bagan	f
191	\N	JS	Air Koryo	f
192	\N	P0	Proflight Zambia	f
193	\N	LK	Air Luxor	f
194	\N	MK	Air Mauritius	f
195	\N	XG	SunExpress	f
196	\N	RE	Aer Arann	f
197	\N	PD	Porter Airlines	t
198	\N	FJ	Fiji Airways	f
199	\N	BD	Cambodia Bayon Airlines	f
200	\N	8D	Astair	f
201	\N	OT	Aeropelican Air Services	f
202	\N	6K	Asian Spirit	f
203	\N	IO	IrAero	t
204	\N	UU	Air Austral	f
205	\N	PS	Ukraine International Airlines	f
206	\N	MS	Egyptair	f
207	\N	5L	Aerosur	f
208	\N	J8	Berjaya Air	f
209	\N	Z3	Avient Aviation	f
210	\N	EC	Avialeasing Aviation Company	f
211	\N	2K	Aerolineas Galapagos (Aerogal)	f
212	\N	TX	Air Caraïbes	f
213	\N	RC	Atlantic Airways	f
214	\N	7G	Star Flyer	t
215	\N	4R	Renfe	f
216	\N	LV	MEGA Maledives Airline	f
217	\N	P7	Small Planet Airline	f
218	\N	BS	British International Helicopters	f
219	\N	KF	Blue1	f
220	\N	JJ	LATAM Brasil	f
221	\N	CH	Bemidji Airlines	f
222	\N	8E	Bering Air	f
223	\N	B2	Belavia Belarusian Airlines	f
224	\N	FB	Bulgaria Air	f
225	\N	GS	Tianjin Airlines	f
226	\N	BF	French Blue	f
227	\N	KC	Air Astana	f
228	\N	OI	Orchid Airlines	f
229	\N	AJ	Aero Contractors	f
230	\N	EN	Air Dolomiti	f
231	\N	YK	Avia Traffic Airline	f
232	\N	TY	Air Caledonie	f
233	\N	VX	Virgin America	t
234	\N	NF	Air Vanuatu	f
235	\N	C9	SkyWise	t
236	\N	8N	Regional Air Services	f
237	\N	HR	China United Airlines	f
238	\N	QI	Cimber Air	f
239	\N	DQ	Coastal Air	f
240	\N	KR	Comores Airlines	f
241	\N	6A	Consorcio Aviaxsa	f
242	\N	OU	Croatia Airlines	f
243	\N	TA	Grupo TACA	f
244	\N	2L	Helvetic Airways	f
245	\N	2T	TruJet	f
246	\N	GF	Gulf Air Bahrain	f
247	\N	XK	Air Corsica	f
248	\N	KB	Druk Air	f
249	\N	HU	Hainan Airlines	f
250	\N	SS	Corsairfly	f
251	\N	CZ	China Southern Airlines	f
252	\N	ZK	Great Lakes Airlines	f
253	\N	DO	Dominicana de Aviaci	f
254	\N	E3	Domodedovo Airlines	f
255	\N	H7	Eagle Air	f
256	\N	QU	East African	f
257	\N	T3	Eastern Airways	f
258	\N	UZ	El-Buraq Air Transport	f
259	\N	EU	Empresa Ecuatoriana De Aviacion	f
260	\N	B8	Eritrean Airlines	f
261	\N	EA	European Air Express	f
262	\N	JN	Excel Airways	f
263	\N	EO	Express One International	f
264	\N	FP	Freedom Air	f
265	\N	CU	Cubana de Aviación	f
266	\N	CX	Cathay Pacific	f
267	\N	ZE	Eastar Jet	t
268	\N	PY	Surinam Airways	f
269	\N	OA	Olympic Air	f
270	\N	GP	APG Airlines	f
271	\N	I7	Paramount Airways	f
272	\N	4G	Gazpromavia	f
273	\N	A9	Georgian Airways	f
274	\N	QB	Georgian National Airlines	f
275	\N	G0	Ghana International Airlines	f
276	\N	G7	GoJet Airlines	f
277	\N	TS	Air Transat	f
278	\N	UD	Hex'Air	f
279	\N	FW	Ibex Airlines	f
280	\N	EY	Etihad Airways	f
281	\N	RZ	Sansa Air	f
282	\N	D2	Severstal Air Company	t
283	\N	LO	LOT Polish Airlines	f
284	\N	IC	Indian Airlines	f
285	\N	D6	Interair South Africa	f
286	\N	IR	Iran Air	f
287	\N	EP	Iran Aseman Airlines	f
288	\N	H8	Latin American Wings	f
289	\N	ID	Batik Air	t
290	\N	WC	Islena De Inversiones	f
291	\N	GI	Itek Air	f
292	\N	JC	JAL Express	f
293	\N	JO	JALways	f
294	\N	6H	Israir	f
295	\N	2M	Maya Island Air	f
296	\N	RQ	Kam Air	f
297	\N	KV	Kavminvodyavia	f
298	\N	M5	Kenmore Air	f
299	\N	Y9	Kish Air	f
300	\N	7K	Kogalymavia Air Company	f
301	\N	GW	Kuban Airlines	f
302	\N	EG	Ernest Airlines	f
303	\N	NG	Lauda Air	f
304	\N	LN	Libyan Arab Airlines	f
305	\N	AT	Royal Air Maroc	f
306	\N	MP	Martinair	f
307	\N	MZ	Merpati Nusantara Airlines	f
308	\N	YV	Mesa Airlines	f
309	\N	MX	Mexicana de Aviaci	f
310	\N	YX	Midwest Airlines	f
311	\N	MY	Midwest Airlines (Egypt)	f
312	\N	8I	Myway Airlines	f
313	\N	YM	Montenegro Airlines	f
314	\N	3R	Moskovia Airlines	f
315	\N	M9	Motor Sich	f
316	\N	8P	Pacific Coastal Airline	f
317	\N	NC	National Jet Systems	f
318	\N	ON	Nauru Air Corporation	f
319	\N	MT	Thomas Cook Airlines	t
320	\N	V7	Volotea	t
321	\N	HK	Yangon Airways	f
322	\N	RA	Nepal Airlines	f
323	\N	EJ	New England Airlines	f
324	\N	U7	Northern Dene Airways	f
325	\N	J3	Northwestern Air	f
326	\N	9B	AccesRail	f
327	\N	NU	Japan Transocean Air	f
328	\N	KE	Korean Air	f
329	\N	O2	Oceanic Airlines	f
330	\N	OY	Omni Air International	f
331	\N	OL	Ostfriesische Lufttransport	f
332	\N	VR	TACV	f
333	\N	PV	PAN Air	f
334	\N	PI	Polar Airlines	f
335	\N	O6	Avianca Brazil	f
336	\N	PU	Plus Ultra Lineas Aereas	f
337	\N	QV	Lao Airlines	f
338	\N	U4	Buddha Air	f
339	\N	8J	Eco Jet	f
340	\N	9E	Pinnacle Airlines	f
341	\N	PH	Polynesian Airlines	f
342	\N	BK	Potomac Air	f
343	\N	IK	Pegas Fly	f
344	\N	O7	Orenburzhye Airline	f
345	\N	7B	Fly Blue Crane	f
346	\N	CG	Airlines PNG	f
347	\N	LM	LoganAir LM	f
348	\N	RH	Republic Express Airlines	f
349	\N	GZ	Air Rarotonga	f
350	\N	FQ	Thomas Cook Airlines	f
351	\N	6O	Orbest	f
352	\N	S4	SATA International	f
353	\N	MQ	American Eagle Airlines	f
354	\N	5M	Sibaviatrans	f
355	\N	ZS	Sama Airlines	f
356	\N	FT	Siem Reap Airways	f
357	\N	5G	Skyservice Airlines	f
358	\N	FS	Servicios de Transportes A	f
359	\N	SD	Sudan Airways	f
360	\N	SJ	Sriwijaya Air	f
361	\N	RB	Syrian Arab Airlines	f
362	\N	SC	Shandong Airlines	f
363	\N	S5	Shuttle America	f
364	\N	BU	Baikotovitchestrian Airlines 	f
365	\N	3P	Tiara Air	f
366	\N	YB	BoraJet	t
367	\N	Q8	Trans Air Congo	f
368	\N	GE	TransAsia Airways	f
369	\N	RL	Royal Falcon	f
370	\N	T5	Turkmenistan Airlines	f
371	\N	LW	Latin American Wings	f
372	\N	NO	Neos Air	t
373	\N	5F	Fly One	t
374	\N	47	88	f
375	\N	DT	TAAG Angola Airlines	f
376	\N	AX	Trans States Airlines	f
377	\N	JD	Beijing Capital Airlines	f
378	\N	A4	Azimuth	f
379	\N	3T	Turan Air	f
380	\N	U5	USA3000 Airlines	f
381	\N	UF	UM Airlines	f
382	\N	US	US Airways	f
383	\N	2W	Welcome Air	f
384	\N	8O	West Coast Air	f
385	\N	IV	Wind Jet	f
386	\N	MF	Xiamen Airlines	f
387	\N	IY	Yemenia	f
388	\N	9Y	Air Kazakhstan	f
389	\N	HZ	Aurora Airlines	f
390	\N	VS	Virgin Atlantic Airways	f
391	\N	S7	S7 Airlines	f
392	\N	KW	Carnival Air Lines	f
393	\N	B7	Uni Air	f
394	\N	YD	Gomelavia	f
395	\N	TM	Air Mozambique	f
396	\N	GM	Germania Flug	f
397	\N	VH	Virgin Pacific	f
398	\N	CN	Canadian National Airways	f
399	\N	L8	Line Blue	f
400	\N	DH	Dennis Sky	f
401	\N	ZZ	Zz	f
402	\N	PZ	LATAM Paraguay	f
403	\N	EQ	TAME	f
404	\N	J4	ALAK	f
405	\N	S6	Salmon Air	f
406	\N	WD	Amsterdam Airlines	f
407	\N	8F	STP Airways	f
408	\N	7Y	Med Airways	f
409	\N	UQ	Skyjet Airlines	f
410	\N	G6	Air Volga	f
411	\N	4L	Euroline	f
412	\N	ZF	Athens Airways	f
413	\N	DZ	Starline.kz	f
414	\N	6P	Gryphon Airlines	f
415	\N	JR	Joy Air	f
416	\N	9H	MDLR Airlines	f
417	\N	TI	Tailwind Airlines	f
418	\N	Q2	Maldivian	f
419	\N	JH	Fuji Dream Airlines	f
420	\N	LZ	belleair	f
421	\N	XZ	Congo Express	f
422	\N	HY	Uzbekistan Airways	f
423	\N	TV	Tibet Airlines	f
424	\N	UR	UTair-Express	f
425	\N	G4	Allegiant Air	t
426	\N	BB	Seaborne Airlines	f
427	\N	MN	Kulula	t
428	\N	H5	I-Fly	f
429	\N	R8	AirRussia	f
430	\N	D1	Domenican Airlines	f
431	\N	C4	LionXpress	f
432	\N	69	Royal European Airlines	f
433	\N	XO	LTE International Airways	f
434	\N	G5	Huaxia	f
435	\N	3B	JobAir	f
436	\N	P8	Air Mekong	f
437	\N	HH	Air Hamburg (AHO)	f
438	\N	NS	Caucasus Airlines	f
439	\N	VO	FlyVLM	f
440	\N	KT	VickJet	f
441	\N	XV	BVI Airways	f
442	\N	ZJ	Zambezi Airlines (ZMA)	f
443	\N	YQ	Polet Airlines (Priv)	f
444	\N	12	12 North	f
445	\N	L6	Mauritania Airlines International	f
446	\N	GY	Colorful Guizhou Airlines	f
447	\N	CE	Chalair	f
448	\N	7L	Sun D'Or	f
449	\N	U3	Avies	f
450	\N	ZA	Access Air	f
451	\N	TD	Atlantis European Airways	f
452	\N	VC	Via Air	f
453	\N	DX	DAT Danish Air Transport	f
454	\N	D3	Daallo Airlines	f
455	\N	IA	Iraqi Airways	f
456	\N	1T	1Time Airline	f
457	\N	FM	Shanghai Airlines	f
458	\N	UH	AtlasGlobal Ukraine	f
459	\N	JK	Spanair	f
460	\N	S3	Santa Barbara Airlines	f
461	\N	JZ	Skyways Express	f
462	\N	K5	SeaPort Airlines	f
463	\N	NB	Sterling Airlines	f
464	\N	VI	Volga-Dnepr Airlines	f
465	\N	VK	Virgin Nigeria Airways	f
466	\N	VG	VLM Airlines	f
467	\N	2Q	Air Cargo Carriers	f
468	\N	OK	Czech Airlines	f
469	\N	5P	Small Planet Airlines	f
470	\N	IE	Solomon Airlines	f
471	\N	6J	Solaseed Air	f
472	\N	4Q	Safi Airlines	f
473	\N	7Q	Pan Am World Airways Dominicana	f
474	\N	V6	VIP Ecuador	f
475	\N	OC	Catovair	f
476	\N	7Z	Halcyonair	f
477	\N	4P	Business Aviation	f
478	\N	UM	Air Zimbabwe	f
479	\N	6Y	SmartLynx Airlines	f
480	\N	AH	Air Algerie	f
481	\N	E4	Elysian Airlines	f
482	\N	7T	Trenitalia	f
483	\N	M4	Mistral Air	f
484	\N	FI	Icelandair	f
485	\N	ZN	Zenith International Airline	f
486	\N	HT	Hellenic Imperial Airways	f
487	\N	Q9	Arik Niger	f
488	\N	YO	TransHolding System	f
489	\N	3F	Fly Colombia ( Interliging Flights )	f
490	\N	HC	Himalayan Airlines	f
491	\N	V9	Star1 Airlines	f
492	\N	ZG	Grozny Avia	f
493	\N	AU	Austral Lineas Aereas	f
494	\N	EV	ExpressJet	f
495	\N	2F	Frontier Flying Service	f
496	\N	9S	Spring Airlines	f
497	\N	K1	Kostromskie avialinii	f
498	\N	B1	Baltic Air lines	f
499	\N	XX	Greenfly	f
500	\N	FU	Felix Airways	f
501	\N	DS	EasyJet (DS)	f
502	\N	YL	Yamal Airlines	f
503	\N	WO	World Airways	f
504	\N	8Z	Wizz Air Hungary	f
505	\N	R5	Jordan Aviation	f
506	\N	HF	Air Cote d'Ivoire	f
507	\N	6B	TUIfly Nordic	f
508	\N	9T	Transwest Air	f
509	\N	AL	Skywalk Airlines	f
510	\N	SR	Swissair	f
511	\N	SP	SATA Air Acores	f
512	\N	RD	Ryan International Airlines	f
513	\N	R4	Rossiya	f
514	\N	NI	Portugalia	f
515	\N	9Q	PB Air	f
516	\N	OJ	Overland Airways	f
517	\N	QO	Origin Pacific Airways	f
518	\N	NW	Northwest Airlines	f
519	\N	1I	NetJets	f
520	\N	DM	Maersk	f
521	\N	MB	MNG Airlines	f
522	\N	VV	Viva Air Peru	f
523	\N	CL	Lufthansa CityLine	f
524	\N	HE	Luftfahrtgesellschaft Walter	f
525	\N	L3	LTU Austria	f
526	\N	WJ	Air Labrador	f
527	\N	3O	Air Arabia Maroc	f
528	\N	1B	Abacus International	f
529	\N	H6	Bulgarian Air Charter	f
530	\N	WA	KLM Cityhopper	f
531	\N	JF	Jetairfly	f
532	\N	QJ	Jet Airways	f
533	\N	QX	Horizon Air	f
534	\N	YS	Régional	f
535	\N	GO	Kuzu Airlines Cargo	f
536	\N	GH	Globus	f
537	\N	PL	Aeroper	f
538	\N	8A	Atlas Blue	f
539	\N	NH	All Nippon Airways	f
540	\N	O8	Siam Air	f
541	\N	NQ	Air Japan	f
542	\N	XR	Corendon Airlines Europe	f
543	\N	U9	Tatarstan Airlines	f
544	\N	QM	Air Malawi	f
545	\N	NX	Air Macau	f
546	\N	4N	Air North Charter - Canada	f
547	\N	2B	Aerocondor	f
548	\N	3G	Atlant-Soyuz Airlines	f
549	\N	V3	Carpatair	f
550	\N	L5	Atlas Atlantique Airlines	f
551	\N	BW	Caribbean Airlines	f
552	\N	QC	Camair-co	f
553	\N	3U	Sichuan Airlines	f
554	\N	WU	Wizz Air Ukraine	f
555	\N	BQ	Buquebus Líneas Aéreas	f
556	\N	YY	Virginwings	f
557	\N	MR	Homer Air	f
558	\N	8U	Afriqiyah Airways	f
559	\N	LF	FlyNordic	f
560	\N	BH	Hawkair	f
561	\N	8H	Heli France	f
562	\N	JB	Helijet	f
563	\N	T4	Hellas Jet	f
564	\N	HW	Hello	f
565	\N	M7	MasAir	f
566	\N	OM	MIAT Mongolian Airlines	f
567	\N	PO	Polar Airlines	f
568	\N	78	Southjet cargo	f
569	\N	4H	United Airways	f
570	\N	W2	Flexflight	f
571	\N	MA	Malév	f
572	\N	AE	Mandarin Airlines	f
573	\N	GV	Grant Aviation	f
574	\N	N5	Skagway Air Service	f
575	\N	5E	SGA Airlines	f
576	\N	AO	Avianova (Russia)	f
577	\N	P4	Aerolineas Sosa	f
578	\N	WQ	PanAm World Airways	f
579	\N	KY	KSY	f
580	\N	CQ	SOCHI AIR	f
581	\N	DN	Senegal Airlines	f
582	\N	8B	BusinessAir	f
583	\N	YR	SENIC AIRLINES	f
584	\N	W5	Mahan Air	f
585	\N	G8	Go Air	t
586	\N	OG	AirOnix	f
587	\N	WH	China Northwest Airlines (WH)	f
588	\N	JA	JetSmart	f
589	\N	5Q	BQB Lineas Aereas	f
590	\N	KG	Royal Airways	f
591	\N	YH	Yangon Airways Ltd.	f
592	\N	FG	Ariana Afghan Airlines	f
593	\N	LB	Air Costa	t
594	\N	4M	LATAM Argentina	f
595	\N	I2	Iberia Express	f
596	\N	ZC	Korongo Airlines	f
597	\N	Q3	SOCHI AIR CHATER	f
598	\N	AN	Ansett Australia	f
599	\N	4K	Askari Aviation	f
600	\N	ZX	Japan Regio	f
601	\N	L4	Luchsh Airlines 	f
602	\N	HI	Papillon Grand Canyon Helicopters	f
603	\N	SO	Salsa d\\\\'Haiti	f
604	\N	VP	VASP	f
605	\N	B5	Fly-SAX	f
606	\N	TJ	Tradewind Aviation	f
607	\N	SW	Air Namibia	f
608	\N	VA	Virgin Australia Airlines	t
609	\N	RG	Rotana Jet	f
610	\N	TU	Tunisair	f
611	\N	GB	BRAZIL AIR	f
612	\N	ES	EuropeSky	f
613	\N	1C	OneChina	f
614	\N	OQ	Chongqing Airlines	f
615	\N	1X	Branson Air Express	f
616	\N	ZY	Ada Air	f
617	\N	C5	CommutAir	f
618	\N	C3	Contact Air	f
619	\N	CS	Continental Micronesia	f
620	\N	0D	Darwin Airline	f
621	\N	SU	Aeroflot Russian Airlines	f
622	\N	DK	Eastland Air	f
623	\N	WK	Edelweiss Air	f
624	\N	GJ	Eurofly Service	f
625	\N	EZ	Evergreen International Airlines	f
626	\N	XE	ExpressJet	f
627	\N	M3	Air Norway	f
628	\N	RF	Florida West International Airways	f
629	\N	FH	Freebird Airlines	f
630	\N	C1	CanXpress	f
631	\N	QA	Click (Mexicana)	f
632	\N	W1	World Experience Airline	f
633	\N	ZQ	Locair	f
634	\N	PA	Airblue	t
635	\N	N1	N1	f
636	\N	3J	Jubba Airways	f
637	\N	H9	Himalaya Airlines	f
638	\N	EM	Empire Airlines	f
639	\N	Y8	Marusya Airways	f
640	\N	4X	Red Jet Mexico	f
641	\N	QY	Red Jet Canada	f
642	\N	E1	Usa Sky Cargo	f
643	\N	GN	GNB Linhas Aereas	f
644	\N	XB	NEXT Brasil	f
645	\N	6U	Air Cargo Germany	f
646	\N	N0	Norte Lineas Aereas	f
647	\N	G1	Indya Airline Group	f
648	\N	T6	Trans Pas Air	f
649	\N	1F	CB Airways UK ( Interliging Flights )	f
650	\N	F1	Fly Brasil	f
651	\N	CB	CCML Airlines	f
652	\N	24	Euro Jet	f
653	\N	2D	Aero VIP (2D)	f
654	\N	Y1	Yellowstone Club Private Shuttle	f
655	\N	H3	Harbour Air (Priv)	f
656	\N	TH	TransBrasil Airlines	f
657	\N	T7	Twin Jet	f
658	\N	A6	Air Alps Aviation (A6)	f
659	\N	BZ	Black Stallion Airways	f
660	\N	RI	Mandala Airlines	f
661	\N	HQ	Thomas Cook Belgium	f
662	\N	KN	China United	f
663	\N	4B	BoutiqueAir	f
664	\N	I6	Air indus	f
665	\N	__	FakeAirline	f
666	\N	NA	Nesma Air	f
667	\N	IZ	Arkia	t
668	\N	KA	Cathay Dragon	f
669	\N	QG	Citilink	t
670	\N	E9	Compagnie Africaine d\\\\'Aviation	f
671	\N	RS	Air Seoul	f
672	\N	DG	CebGo	f
673	\N	Q6	Volaris Costa Rica	f
674	\N	IW	Wings Air	f
675	\N	HD	Air Do	t
676	\N	CI	China Airlines	f
677	\N	VB	VivaAerobus	t
678	\N	5H	Fly540	t
679	\N	F8	Flair Airlines	f
680	\N	K6	Cambodia Angkor Air	f
681	\N	DA	Dana Air	f
682	\N	JE	Mango	t
683	\N	YT	Yeti Airways	f
684	\N	OB	Boliviana de Aviación	t
685	\N	TG	Thai Airways International	f
686	\N	K8	Kan Air	f
687	\N	RO	Tarom	f
688	\N	WW	WOW air	t
689	\N	77	Southjet connect	f
690	\N	9X	Southern Airways Express	t
691	\N	CC	CM Airlines	f
692	\N	OH	Comair	f
693	\N	9L	Colgan Air	f
694	\N	CJ	BA CityFlyer	f
695	\N	O1	Orbit Airlines Azerbaijan	f
696	\N	5K	Hi Fly (5K)	f
697	\N	NJ	Nordic Global Airlines	f
698	\N	20	Air Salone	f
699	\N	EE	RegionalJet	f
700	\N	QL	Laser Air	t
701	\N	RK	Air Afrique	f
702	\N	76	Southjet	f
703	\N	OS	Austrian Airlines	f
704	\N	FR	Ryanair	t
705	\N	SA	South African Airways	f
706	\N	GR	Aurigny Air Services	f
707	\N	UX	Air Europa	f
708	\N	VN	Vietnam Airlines	f
709	\N	WN	Southwest Airlines	t
710	\N	TK	Turkish Airlines	f
711	\N	9K	Cape Air	t
712	\N	KM	Air Malta	f
713	\N	AA	American Airlines	f
714	\N	YE	Yan Air	f
715	\N	Z9	Bek Air	f
716	\N	XC	Corendon	t
717	\N	P6	Pascan Aviation	f
718	\N	YU	EuroAtlantic Airways	f
719	\N	9M	Central Mountain Air	f
720	\N	EF	EasyFly	t
721	\N	KI	KrasAvia	t
722	\N	7N	Pawa Dominicana	f
723	\N	6T	Air Mandalay	t
724	\N	SM	Air Cairo	t
725	\N	XL	LATAM Ecuador	f
726	\N	P5	Wingo airlines	f
727	\N	2C	SNCF	f
728	\N	A3	Aegean	t
729	\N	NK	Spirit Airlines	t
730	\N	2J	Air Burkina	f
731	\N	BP	Air Botswana	f
732	\N	V8	ATRAN Cargo Airlines	f
733	\N	6F	Primera Air Nordic	t
734	\N	CW	Air Marshall Islands	f
735	\N	IG	Meridiana	t
736	\N	MW	Mokulele Flight Service	f
737	\N	DL	Delta Air Lines	f
738	\N	RV	Caspian Airlines	f
739	\N	C0	Centralwings	f
740	\N	VZ	Thai Vietjet	f
741	\N	IS	Island Airlines	f
742	\N	IF	Islas Airways	f
743	\N	R6	RACSA	f
744	\N	RW	Republic Airlines	f
745	\N	C2	CanXplorer	f
746	\N	T2	Thai Air Cargo	f
747	\N	Q5	40-Mile Air	f
748	\N	V5	Danube Wings (V5)	f
749	\N	SH	Sharp Airlines	f
750	\N	LC	Varig Log	f
751	\N	XF	Vladivostok Air	f
752	\N	7W	Wayraper	f
753	\N	F7	Etihad Regional	f
754	\N	S2	Air Sahara	f
755	\N	V2	Vision Airlines	f
756	\N	8V	Astral Aviation	f
757	\N	8T	Air Tindi	f
758	\N	UB	Myanmar National Airlines	f
759	\N	YN	Air Creebec	f
760	\N	PB	Provincial Airlines	f
761	\N	7V	Federai Airlines	f
762	\N	V4	Vieques Air Link	f
763	\N	P1	Publiccharters.com	f
764	\N	0V	VASCO	f
765	\N	ZD	EWA Air	f
766	\N	4W	Allied Air	f
767	\N	7J	Tajik Air	t
768	\N	N6	Nomad Aviation	f
769	\N	F2	Safarilink Aviation	f
770	\N	WG	Sunwing	t
771	\N	WV	Aero VIP	f
772	\N	J1	One Jet	f
773	\N	ST	Germania	t
774	\N	UL	SriLankan Airlines	f
775	\N	ML	Air Mediterranee	t
776	\N	JT	Lion Air	t
777	\N	DB	Brit Air	f
778	\N	R3	Yakutia Airlines	t
779	\N	VJ	VietJet Air	t
780	\N	DR	Ruili Airlines	t
781	\N	DV	Scat Air	t
782	\N	FA	Fly Safair	t
783	\N	PN	West Air China	t
784	\N	JX	Jambojet	t
785	\N	8L	Lucky air	t
786	\N	LX	Swiss International Air Lines	f
787	\N	K9	KrasAvia (old iata)	t
788	\N	5U	Transportes Aéreos Guatemaltecos	f
789	\N	XY	flynas	t
790	\N	J7	Greenland Express	t
791	\N	SG	Spicejet	t
792	\N	5W	WESTBahn	f
793	\N	3K	Jetstar Asia Airways	f
794	\N	Z6	Dniproavia	t
795	\N	EL	Ellinair	t
796	\N	MJ	Mihin Lanka	t
797	\N	5C	Nature Air	t
798	\N	9N	Tropic Air Limited	f
799	\N	HX	Hong Kong Airlines	t
800	\N	AW	RAVSA Venezolana	t
801	\N	9C	Spring Airlines	t
802	\N	G9	Air Arabia	t
803	\N	YZ	Alas Uruguay	f
804	\N	5Z	Cem Air	t
805	\N	BR	EVA Air	t
806	\N	NL	Shaheen Air International	t
807	\N	ZB	Monarch	t
808	\N	6R	Alrosa	t
809	\N	AB	Air Berlin	t
810	\N	9V	Avior Airlines	f
811	\N	5O	ASL Airlines France	f
812	\N	EB	Wamos Air	t
813	\N	CA	Air China	f
814	\N	WS	WestJet	t
815	\N	OR	TUI Airlines Netherlands	t
816	\N	GT	Flyvista	t
817	\N	7I	Insel Air	t
818	\N	DP	Pobeda	t
819	\N	BC	Skymark Airlines	t
820	\N	BL	Jetstar Pacific	f
821	\N	5N	NordAvia	t
822	\N	S8	SkyWise	t
823	\N	VF	Valuair	f
824	\N	W4	LC Perú	t
825	\N	KL	KLM Royal Dutch Airlines	f
826	\N	JW	Vanilla Air	t
827	\N	2G	Angara airlines	t
828	\N	Y5	Golden Myanmar Airlines	t
829	\N	7R	Rusline	t
830	\N	LJ	Jin Air	t
831	\N	DC	Sverigeflyg	t
832	\N	WB	Rwandair Express	f
833	\N	8R	Sol Líneas Aéreas	t
834	\N	I4	I-Fly	f
835	\N	8M	Myanmar Airways	f
836	\N	G3	Gol Transportes Aéreos	t
837	\N	PG	Bangkok Airways	t
838	\N	OX	Orient Thai Airlines	t
839	\N	NZ	Air New Zealand	t
840	\N	VQ	Novoair	t
841	\N	X9	Fake Airline	f
842	\N	BI	Royal Brunei Airlines	f
843	\N	BT	airBaltic	t
844	\N	BE	flybe	t
845	\N	XN	Xpressair	f
846	\N	K7	Air KBZ	f
847	\N	MO	Calm Air	t
848	\N	SI	Blue Islands	f
849	\N	MM	Peach Aviation	t
850	\N	C7	Cinnamon Air	t
851	\N	YJ	Asian Wings Air	t
852	\N	OZ	Asiana Airlines	f
853	\N	OD	Malindo Air	t
854	\N	ED	Airblue	t
855	\N	KX	Cayman Airways	f
856	\N	7H	Ravn Alaska	t
857	\N	CO	Cobalt Air	f
858	\N	RX	Regent Airways	t
859	\N	BX	Air Busan	t
860	\N	KQ	Kenya Airways	f
861	\N	ME	Middle East Airlines	f
862	\N	CM	Copa Airlines	f
863	\N	9W	Jet Airways	f
864	\N	IX	Air India Express	t
865	\N	BY	TUI Airways	t
866	\N	Z8	Amaszonas	t
867	\N	KU	Kuwait Airways	f
868	\N	3S	Air Antilles Express	f
869	\N	6W	Saratov Aviation Division	t
870	\N	PW	Precision Air	f
871	\N	TP	TAP Portugal	f
872	\N	ZV	V Air	t
873	\N	YC	Yamal Air	t
874	\N	JL	Japan Airlines	f
875	\N	HO	Juneyao Airlines	t
876	\N	SN	Brussels Airlines	f
877	\N	PF	Primera Air	t
878	\N	QS	SmartWings	t
879	\N	IB	Iberia Airlines	f
880	\N	B0	Aws express	f
881	\N	TF	Braathens Regional Airlines	f
882	\N	BM	BMI regional	f
883	\N	SK	SAS	f
884	\N	AQ	9 Air	t
885	\N	P9	Peruvian Airlines	t
886	\N	DD	Nok Air	t
887	\N	PK	Pakistan International Airlines	f
888	\N	TN	Air Tahiti Nui	f
889	\N	JP	Adria Airways	f
890	\N	9R	SATENA	t
891	\N	VW	Aeromar	f
892	\N	GA	Garuda Indonesia	f
893	\N	JV	Bearskin Lake Air Service	f
894	\N	A2	Astra Airlines	f
895	\N	AF	Air France	f
896	\N	7M	MAYAir	f
897	\N	WP	Island Air	f
898	\N	3M	Silver Airways	t
899	\N	KD	Kalstar Aviation	f
900	\N	CF	City Airline	f
901	\N	3Q	Yunnan Airlines	f
902	\N	CP	Canadian Airlines	f
903	\N	MH	Malaysia Airlines	t
904	\N	ZI	Aigle Azur	f
905	\N	TR	Scoot	t
906	\N	D8	Norwegian International	f
907	\N	QW	Blue Wings	f
908	\N	B3	Bellview Airlines	f
909	\N	TL	Airnorth	t
910	\N	B6	JetBlue Airways	t
911	\N	TO	Transavia France	f
912	\N	N4	Nordwind Airlines	f
913	\N	KP	ASKY Airlines	f
914	\N	QK	Air Canada Jazz	f
915	\N	Q7	SkyBahamas Air	f
916	\N	BG	Biman Bangladesh Airlines	t
917	\N	AI	Air India Limited	f
918	\N	HV	Transavia	t
919	\N	UE	Nasair	f
920	\N	AM	AeroMéxico	f
921	\N	UO	Hong Kong Express Airways	t
922	\N	RP	Chautauqua Airlines	f
923	\N	Z2	Philippines AirAsia	f
924	\N	AD	Azul	t
925	\N	IP	Island Spirit	f
926	\N	UK	Air Vistara	f
927	\N	LY	El Al Israel Airlines	t
928	\N	3H	AirInuit	t
929	\N	R2	Orenburg Airlines	t
930	\N	UA	United Airlines	f
931	\N	3L	Intersky	t
932	\N	2Z	Passaredo	t
933	\N	UT	UTair	t
934	\N	X3	TUIfly	t
935	\N	UP	Bahamasair	t
936	\N	J9	Jazeera Airways	t
937	\N	U2	easyJet	t
938	\N	UN	Transaero Airlines	t
939	\N	I5	AirAsia India	f
940	\N	TW	Tway Airlines	t
941	\N	FY	Firefly	t
942	\N	XJ	Thai AirAsia X	f
943	\N	XT	Indonesia AirAsia X	f
944	\N	AZ	Alitalia	f
945	\N	TZ	Scoot - old	t
946	\N	EW	Eurowings	t
947	\N	WM	Windward Islands Airways	f
948	\N	4U	germanwings	t
949	\N	JQ	Jetstar Airways	t
950	\N	SL	Thai Lion Air	t
951	\N	H2	Sky Airline	t
952	\N	TB	tuifly.be	t
953	\N	AR	Aerolineas Argentinas	f
954	\N	DE	Condor	t
955	\N	TT	Tiger Airways Australia	t
956	\N	W6	Wizzair	t
957	\N	FC	VivaColombia	t
958	\N	5J	Cebu Pacific	t
959	\N	PC	Pegasus	t
960	\N	F9	Frontier Airlines	t
961	\N	FD	Thai AirAsia	f
962	\N	QZ	Indonesia AirAsia	f
963	\N	D7	AirAsia X	f
964	\N	PQ	Philippines AirAsia	f
965	\N	VY	Vueling	t
966	\N	DY	Norwegian	t
967	\N	LS	Jet2	t
968	\N	BV	Blue Panorama	t
969	\N	AK	AirAsia	t
970	\N	4O	Interjet (ABC Aerolineas)	t
971	\N	1D	Aerolinea de Antioquia	t
972	\N	FO	Flybondi	t
\.


--
-- Name: airlines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: suricactus
--
