import EventEmmiter from 'events';

// import logger from 'tb-logger';
export const TYPE_USER = Symbol('UserError');
export const TYPE_PEER = Symbol('PeerError');
export const TYPE_APP = Symbol('AppError');
export const TYPE_SYS = Symbol('SysError');
export const TYPE_CONFIG = Symbol('ConfigError');
export const TYPE_UNKNOWN = Symbol('UnknownError');

const LOG_ERROR = 'ERROR';
const LOG_INFO = 'INFO';
const LOG_WARN = 'WARN';

export const ErrorEmitter = new EventEmmiter();

export class CustomError extends Error {
  constructor (code, msg, opts) {
    super();

    if (msg && typeof msg === 'object') {
      if (opts != null) { throw new CustomError('10000', 'Message set to `object`'); }

      [msg, opts] = [code, msg];
    }

    this.code = code;
    this.msg = msg;

    ErrorEmitter.emit('error', this);
  }
}

export class SysError extends CustomError {
	constructor () {
		super();
		this.type = TYPE_SYS;
		this.creationLogLevel = LOG_ERROR;
		this.unhandledLogLevel = LOG_ERROR;
	}
}

export class AppError extends CustomError {
	constructor () {
		super();
		this.type = TYPE_APP;
		this.creationLogLevel = LOG_ERROR;
		this.unhandledLogLevel = LOG_ERROR;
	}
}

export class PeerError extends CustomError {
	constructor () {
		super();
		this.type = TYPE_PEER;
		this.creationLogLevel = LOG_WARN;
		this.unhandledLogLevel = LOG_ERROR;
	}
}

export class UserError extends CustomError {
	constructor () {
		super();
		this.type = TYPE_USER;
	}
}
