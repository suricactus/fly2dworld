import { AppError, SysError, PeerError, UserError } from './errors';

const ASSERTS_DISABLED = false;

export class AppAssertionError extends AppError {}

export class SystemAssertionError extends SysError {}

export class PeerCheckError extends PeerError {}

export class UserCheckError extends UserError {}

const assertFactory = ErrorClass => {
  return function assert(condition, code, msg, opts) {
    if (ASSERTS_DISABLED) return;

    if (condition) return;

    const err = new ErrorClass(code, msg, opts);

    throw err;
  };
};

export const assert = assertFactory(AppAssertionError);
export const assertSystem = assertFactory(SystemAssertionError);
export const checkPeer = assertFactory(PeerCheckError);
export const checkUser = assertFactory(UserCheckError);

