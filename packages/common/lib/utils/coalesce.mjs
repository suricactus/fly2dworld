export default function () {
  for (let i = 0, l = arguments.length; i < l; i++) {
    if (arguments[i] !== null && arguments[i] !== undefined) {
      return arguments[i];
    }
  }

  return null;
}
