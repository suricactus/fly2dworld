import config from 'config';

class CustomError extends Error {
  constructor (opts) {
    if (opts instanceof CustomError) {
      return opts;
    }

    if (typeof opts === 'string') {
      opts = {
        msg: opts,
      };
    }

    const debug = config.get('debug');

    let msg = opts.msg || config.get('error.defaultMsg');

    if (debug && opts instanceof Error) {
      msg = msg || opts.message;
    }

    super(msg);

    this._opts = opts;
    this._orig = (opts instanceof Error) ? opts : undefined;

    this.msg = msg;
    this.message = this._orig ? this._orig.message : msg;
    this.code = opts.code || config.get('error.defaultCode');
    this.status = opts.status || config.get('error.defaultStatus');
  }

  getObject () {
    const debug = config.get('debug');
    const result = {
      msg: this.msg,
      code: this.code,
      details: this.details,
    };

    if (debug) {
      result.stack = this.stack;
    }

    return result;
  }
}

export default CustomError;
