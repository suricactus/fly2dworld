import config from 'config';
import CustomError from './CustomError';

class PermissionError extends CustomError {
  constructor (opts) {
    opts.msg = config.get('error.permissionMsg');
    opts.code = config.get('error.permissionCode');
    opts.status = config.get('error.permissionStatus');
    opts.message = opts.msg;

    super(opts);
  }
}

export default PermissionError;
