import config from 'config';

import CustomError from '../utils/CustomError';
import logger from '../logger';


async function errorHandler(ctx, next) {
  try {
    await next();
  } catch (origError) {
    logger.error({
      error: origError,
    }, 'Unhandled error encountered!');
    let err;

    try {
      err = new CustomError(origError);
    } catch (e) {
      logger.error({
        error: e
      }, 'Unable to normalize error!');

      ctx.status = 500;
      ctx.body = {
        message: config.get('error.defaultMsg'),
        code: config.get('error.defaultCode')
      };

      return;
    }

    ctx.status = err.status || 500;
    ctx.body = err.getObject();

    ctx.app.emit('error', origError, ctx);
  }
}

export default () => {
  return errorHandler;
}