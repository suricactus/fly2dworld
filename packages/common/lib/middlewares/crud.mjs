import Router from 'koa-router';
import config from 'config';
import coalesce from '../utils/coalesce';

export const defaultMethods = {
  GET: 'managed',
  POST: 'managed',
  PUT: 'managed',
  DELETE: 'managed',
};

export const pgCrudFactory = (queriesFactory, configuredMethods = {}, opts = {}) => ({
  initialize: async (ctx, next) => {
    ctx.availableMethods = Object.assign({}, defaultMethods, configuredMethods);
    ctx.queries = queriesFactory(ctx);

    await next();
  },

  get: async (ctx, next) => {
    if (ctx.availableMethods.GET) {
      // query may contains this params: ['limit', 'page', 'filter', 'sort', 'order'];
      const query = ctx.request.query;
      let limit = +coalesce(query.limit, config.get('crud.select.defaultLimit'));

      limit = isNaN(limit)
        ? config.get('crud.select.defaultLimit')
        : Math.min(limit, config.get('crud.select.maxLimit'));

      let offset = ((Math.max(+query.page, 1) || 1) - 1) * limit;

      ctx.body = await ctx.db.query(ctx.queries.select({
        limit: limit,
        offset: offset,
        filter: query.filter,
        sort: query.sort,
        sortDir: query.order,
      }));

      const totalCount = (await ctx.db.query(ctx.queries.countAll())).count;
      ctx.set('X-Total-Count', totalCount);
      ctx.set('Content-Range', `items ${offset}-${ctx.body.length}/${totalCount}`);
    }

    if (ctx.availableMethods.GET !== 'managed') {
      await next();
    }
  },

  getById: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.GET) {
      try {
        ctx.body = await ctx.db.query(ctx.queries.selectOne({ id }));
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
        } else {
          throw e;
        }
      }
    }

    if (ctx.availableMethods.GET !== 'managed') {
      await next();
    }
  },

  post: async (ctx, next) => {
    if (ctx.availableMethods.POST) {
      const data = ctx.data || ctx.request.body;
      ctx.body = await ctx.queries.insertOne(data);
    }

    if (ctx.availableMethods.POST !== 'managed') {
      await next();
    }
  },

  postMulti: async (ctx, next) => {
    if (ctx.availableMethods.POST) {
      const data = ctx.data || ctx.request.body;
      ctx.body = await ctx.queries.batchInsert(data);
    }

    if (ctx.availableMethods.POST !== 'managed') {
      await next();
    }
  },

  delete: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.DELETE) {
      try {
        ctx.body = await ctx.queries.deleteOne(id);
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
        } else {
          throw e;
        }
      }
    }

    if (ctx.availableMethods.DELETE !== 'managed') {
      await next();
    }
  },

  deleteMulti: async (ctx, next) => {
    if (ctx.availableMethods.DELETE) {
      const ids = ctx.query.id;
      ctx.body = await ctx.queries.batchDelete(ids);
    }

    if (ctx.availableMethods.DELETE !== 'managed') {
      await next();
    }
  },

  put: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.PUT) {
      const data = ctx.data || ctx.request.body;
      let modifiedEntity;
      try {
        modifiedEntity = await ctx.queries.updateOne(id, data);
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
          return;
        }

        throw e;
      }
      ctx.body = modifiedEntity;
    }

    if (ctx.availableMethods.PUT !== 'managed') {
      await next();
    }
  },
});

/**
 * Creates new CRUD API endpoints
 * @param  {Object} queriesFactory    Object containing all CRUD related DB queries
 * @param  {Object} configuredMethods Object containing automatically managed queries
 * @return {Koa}                      Koa app
 */
export default (queriesFactory, configuredMethods = {}) => {
  const router = new Router();
  const pgCrud = pgCrudFactory(queriesFactory, configuredMethods);

  // GET /
  router.get('/', pgCrud.initialize, pgCrud.get);

  // GET /:id
  router.get('/:id', pgCrud.initialize, pgCrud.getById);

  // POST /
  router.post('/', pgCrud.initialize, pgCrud.post);

  // POST /multi
  router.post('/multi', pgCrud.initialize, pgCrud.postMulti);

  // DELETE /
  router.delete('/:id', pgCrud.initialize, pgCrud.delete);

  // DELETE /multi
  router.delete('/multi', pgCrud.initialize, pgCrud.deleteMulti);

  // PUT /:id
  router.put('/:id', pgCrud.initialize, pgCrud.put);

  return router;
};
