import logger from '../logger';
import rbac from '../rbac';
import PermissionError from '../utils/PermissionError';

export const buildPerm = (httpMethod, resource, opts = {}) => {
  if(typeof opts.prefix !== 'string') {
    throw new Error('Missing required `prefix` property');
  }

  let path = resource.replace(opts.prefix, '');

  path = path.replace('//', '/');
  path = path.replace(/(.+)\/$/, '$1');

  return `${httpMethod.toUpperCase()} ${path}`;
};


export default async(ctx, next) => {
  console.log(444,ctx.state)
  const perm = buildPerm(ctx.method, ctx._matchedRoute, { prefix: ctx.router.opts.prefix });
  const login = ctx.state.user
    ? ctx.state.user.user.login
    : 'guest';
  const hasPermission = await rbac.check(login, perm);

  logger.info('Checking permission "%s" for "%s"', perm, login, hasPermission);

  if(!hasPermission) {
    throw new PermissionError({
      login: login,
      perm: perm,
    });
  }

  await next();
};