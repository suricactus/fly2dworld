import dbConnect from 'sca-db';

export default () => {
  const db = dbConnect();

  return db;
};
