import initLogger from 'sca-logger';

const logger = initLogger();

export default logger;
