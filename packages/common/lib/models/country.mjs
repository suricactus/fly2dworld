import * as pgQueries from 'pg-common-queries';

const tableName = 'countries';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
