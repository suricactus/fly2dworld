import * as pgQueries from 'pg-common-queries';

const tableName = 'regions';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
