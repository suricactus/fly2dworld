import * as pgQueries from 'pg-common-queries';

const tableName = 'cities';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
