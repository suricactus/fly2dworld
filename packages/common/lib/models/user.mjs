import * as pgQueries from 'pg-common-queries';

const tableName = 'trip';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
