import * as pgQueries from 'pg-common-queries';

const tableName = 'airports';

const crud = pgQueries.crud({
  table: tableName,
});

crud.selectSignificant = (...args) => {
  return pgQueries.select({
    table: 'significant_airports_vw',
  })(...args);
};

crud.selectSignificantS = (...args) => {
  return pgQueries.select({
    returnCols: ['iata_code', 'name', 'lat', 'lng', 'wikipedia_link', 'country_code', 'city_name', 'is_capital'],
    table: 'significant_airports_vw',
  })(...args);
};

export default crud;
