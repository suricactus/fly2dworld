import * as pgQueries from 'pg-common-queries';

const tableName = 'connections';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
