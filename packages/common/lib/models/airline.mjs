import * as pgQueries from 'pg-common-queries';

const tableName = 'airlines';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
