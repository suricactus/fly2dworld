import * as pgQueries from 'pg-common-queries';

const tableName = 'api_providers';

const crud = pgQueries.crud({
  table: tableName,
  returnCols: ['*'],
  searchableCols: [
    'is_enabled',
  ],
});

export default crud;
