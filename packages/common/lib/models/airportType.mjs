import * as pgQueries from 'pg-common-queries';

const tableName = 'airport_types';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
