import * as pgQueries from 'pg-common-queries';

const tableName = 'search_params';

const crud = pgQueries.crud({
  table: tableName,
  returnCols: ['*'],
  writableCols: [
    'search_condition_id',
    'issued_at',
    'sort',
    'is_roundtrip',
    'is_direct_flight_only',
    'depart_origin_min_date',
    'depart_origin_max_date',
    'return_origin_min_date',
    'return_origin_max_date',
    'stay_duration_min_d',
    'stay_duration_max_d',
    'adults',
    'children',
    'infants',
    'max_flight_duration_m',
    'max_stopovers',
    'stopover_min_time_m',
    'stopover_max_time_m',
    'depart_origin_time_min_m',
    'depart_origin_time_max_m',
    'arrive_origin_time_min_m',
    'arrive_origin_time_max_m',
    'depart_return_time_min_m',
    'depart_return_time_max_m',
    'arrive_return_time_min_m',
    'arrive_return_time_max_m',
    'depart_fly_days',
    'return_fly_days',
    'airlines_list',
    'is_airlines_list_exclusion',
    'airports_list',
    'is_airports_list_exclusion',
    'price_min',
    'price_max',
  ],
});

crud.getCurrentlyActive = () => `

  SELECT *
  FROM search_params
  WHERE issued_at::date = now()::date

`;

crud.appendUpdateTimestamp = ids => `

  UPDATE search_params
  SET update_timestamps = update_timestamps || 'now'::timestamp
  WHERE id IN (${ids.join(', ')})

`;

export default crud;
