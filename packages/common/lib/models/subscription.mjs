import * as pgQueries from 'pg-common-queries';

const tableName = 'search_conditions';
const crud = pgQueries.crud({
  table: tableName,
});

crud.selectUnprocessed = (byDate = 'now') => ({
  sql_old: `

    WITH prev_max_date AS (
        SELECT p.search_condition_id AS id, MAX(p.issued_at) AS max_issued_at
        FROM search_params p
        WHERE p.issued_at < $/byDate/::date - '1 days'::interval
        GROUP BY 1
      ), curr_max_date AS (
        SELECT p.search_condition_id AS id, MAX(p.issued_at) AS max_issued_at
        FROM search_params p
        WHERE TRUE -- p.issued_at < $/byDate/::date
          AND p.issued_at >= $/byDate/::date - '1 days'::interval
        GROUP BY 1
      )
      SELECT c.*
      FROM search_conditions c
      LEFT JOIN prev_max_date pmd ON c.id = pmd.id
      LEFT JOIN curr_max_date cmd ON c.id = cmd.id
      WHERE is_enabled = TRUE
        AND cmd.id IS NULL
        AND (pmd.id IS NOT NULL OR (SELECT count(*) FROM prev_max_date) = 0)

  `,
  sql: `

    WITH curr_max_date AS (
        SELECT p.search_condition_id AS id, MAX(p.issued_at) AS max_issued_at
        FROM search_params p
        WHERE p.issued_at < $/byDate/
          AND p.issued_at >= $/byDate/::date
        GROUP BY 1
    )
    SELECT c.*
      FROM search_conditions c
      LEFT JOIN curr_max_date cmd ON c.id = cmd.id
      WHERE is_enabled = TRUE
        AND cmd.id IS NULL;

  `,
  parameters: {
    byDate,
  },
});

export default crud;
