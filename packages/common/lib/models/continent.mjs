import * as pgQueries from 'pg-common-queries';

const tableName = 'continents';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
