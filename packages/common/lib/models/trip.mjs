import * as pgQueries from 'pg-common-queries';

const tableName = 'trips';

const crud = pgQueries.crud({
  table: tableName,
});

export default crud;
