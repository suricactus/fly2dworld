import config from 'config';
import RBAC from 'rbac-a';

const rbac = new RBAC({
  provider: new RBAC.providers.JsonProvider(config.get('RBAC_RULES')),
});

export default rbac;
