module.exports = {
  debug: true,
  db: {
    user: 'api',
    password: '123',
    database: 'fly2dworld',
    // host: 'localhost',
    // port
  },
  logger: {
    outputMode: 'short',
    name: 'api',
    src: 'src',
  },
  error: {
    defaultMsg: 'Service is temporary unavailable. Please try again several minutes later.',
    defaultCode: '500',
    defaultStatus: 500,
    permissionMsg: 'You don not have permission to access',
    permissionCode: '401',
    permissionStatus: 401,
  },
};
